plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}


dependencies {
    implementation("com.google.apis:google-api-services-androidpublisher:v3-rev20240222-2.0.0")
    implementation("com.google.auth:google-auth-library-oauth2-http:1.18.0")
}
