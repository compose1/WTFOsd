plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.google.apis:google-api-services-androidpublisher:v3-rev20240222-2.0.0")
    implementation("com.google.auth:google-auth-library-oauth2-http:1.18.0") // newest version 1.19.0 notworking!
}
