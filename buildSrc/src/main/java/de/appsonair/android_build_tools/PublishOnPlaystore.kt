package de.appsonair.android_build_tools

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.FileContent
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.androidpublisher.AndroidPublisher
import com.google.api.services.androidpublisher.AndroidPublisherScopes
import com.google.api.services.androidpublisher.model.AppEdit
import com.google.api.services.androidpublisher.model.Bundle
import com.google.api.services.androidpublisher.model.DeobfuscationFile
import com.google.api.services.androidpublisher.model.DeobfuscationFilesUploadResponse
import com.google.api.services.androidpublisher.model.LocalizedText
import com.google.api.services.androidpublisher.model.Track
import com.google.api.services.androidpublisher.model.TrackRelease
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.ServiceAccountCredentials
import java.io.File

enum class ReleaseStatus(
    /** The API name of the status. */
    val publishedName: String,
) {
    /** The release is live. */
    COMPLETED("completed"),
    /** The release is in draft mode. */
    DRAFT("draft"),
    /** The release was aborted. */
    HALTED("halted"),
    /** The release is still being rolled out. */
    IN_PROGRESS("inProgress")
}

class PublisherEditDsl(
    service: AndroidPublisher,
    private val appEdit: AppEdit,
    private val packageName: String
) {
    private val edits = service.Edits()

    /**
     * Uploads a new app bundle to the edit.
     * @see https://developers.google.com/android-publisher/api-ref/rest/v3/edits.bundles/upload
     */
    fun uploadBundle(bundleFile: File): Bundle {
        val inputStreamContent = FileContent("application/octet-stream", bundleFile)
        return edits.bundles()
            .upload(packageName, appEdit.id, inputStreamContent)
            .execute()
    }

    /**
     * Updates a track.
     * In this case a new draft release will be created.
     *
     * @see https://developers.google.com/android-publisher/api-ref/rest/v3/edits.tracks/update
     */
    fun trackRelease(
        versionCode: Int,
        releaseName: String,
        releaseNotesUs: String,
        trackName: String = "internal"
    ): Track {
        val trackRelease = TrackRelease().apply {
            name = releaseName
            versionCodes = mutableListOf(versionCode.toLong())
            status = ReleaseStatus.DRAFT.publishedName
            releaseNotes = listOf(
                LocalizedText().setText(releaseNotesUs).setLanguage("en-US")
            )
        }
        val track = Track().setReleases(listOf(trackRelease))
        return edits.Tracks()
            .update(packageName, appEdit.id, trackName, track)
            .execute()
    }
}

class PublishOnPlayStore(
    private val packageName: String,
    apiAccessJsonFile: File,
    applicationName: String = "CI publisher"
) {
    private val credential = ServiceAccountCredentials
        .fromStream(apiAccessJsonFile.inputStream())
        .createScoped(AndroidPublisherScopes.ANDROIDPUBLISHER)
    private val service = AndroidPublisher.Builder(
        GoogleNetHttpTransport.newTrustedTransport(),
        GsonFactory.getDefaultInstance(),
        HttpCredentialsAdapter(credential)
    ).setApplicationName(applicationName).build()
    private val edits = service.Edits()

    fun insert(block: PublisherEditDsl.() -> Unit): AppEdit {
        val edit = insert()
        val dsl = PublisherEditDsl(service, edit, packageName)
        try {
            block(dsl)
            return commit(edit)
        } catch (err: Throwable) {
            delete(edit)
            println("Delete edit ${edit.id}")
            throw err
        }
    }

    /**
     * Creates a new edit for an app.
     * @see https://developers.google.com/android-publisher/api-ref/rest/v3/edits/insert
     */
    private fun insert() = edits.insert(packageName, null).execute()

    /**
     * Commits an edit.
     * @see https://developers.google.com/android-publisher/api-ref/rest/v3/edits/commit
     */
    private fun commit(appEdit: AppEdit) = edits.commit(packageName, appEdit.id).execute()

    /**
     * Deletes an edit.
     * @see https://developers.google.com/android-publisher/api-ref/rest/v3/edits/delete
     */
    private fun delete(appEdit: AppEdit) = edits.delete(packageName, appEdit.id).execute()

    /**
     * Download all generated universal apks from a previously committed release.
     * Normally this is just one apk. But the api suggests that it could be more.
     * So if there is more than one apk the fileName gets a postfix with an index.
     */
    fun downloadGeneratedUniversalApks(
        versionCode: Int,
        outputFolder: File,
        fileName: String = "universal_$versionCode"
    ) {
        val genApks = service.Generatedapks()
        val apkList = genApks.list(packageName, versionCode).execute()
        apkList.generatedApks.forEachIndexed { index, generatedApksPerSigningKey ->
            val downloadId = generatedApksPerSigningKey.generatedUniversalApk.downloadId
            println("Apk id:$downloadId")
            val name = if (index > 0) "${fileName}_${index + 1}.apk" else "$fileName.apk"
            outputFolder.mkdirs()
            val file = File(outputFolder, name)
            genApks.download(packageName, versionCode, downloadId)
                .executeMediaAndDownloadTo(file.outputStream())
            println("Downloaded: $name")
        }
    }
}

/**
 * Sample usage
 */
fun main() {
    println("Hello world")

    val publishOnPlayStore = PublishOnPlayStore(
        packageName = "de.appsonair.wtf.osdplayer",
        apiAccessJsonFile = File("local_data/api-access.json")
    )

    var versionCode = 0
    val editResult = publishOnPlayStore.insert {
        val bundle = uploadBundle(File("release/app-release.aab"))
        println("Apk uploaded: $bundle")
        versionCode = bundle.versionCode
        //uploadDeobfuscationFiles(File("release/mapping.txt"), versionCode)
        val track = trackRelease(
            versionCode = bundle.versionCode,
            releaseName = "Mango release 50",
            releaseNotesUs = "Release notes test US"
        )
        println("Update track: $track")
    }
    println("App edit has committed: ${editResult.id}")

    publishOnPlayStore.downloadGeneratedUniversalApks(
        versionCode = versionCode,
        outputFolder = File("release")
    )

}