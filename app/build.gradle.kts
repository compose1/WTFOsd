import org.gradle.configurationcache.extensions.capitalized
import java.io.ByteArrayOutputStream
import java.util.Properties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

val composeCompilerVersion = "1.5.10"

fun String.runCommand(currentWorkingDir: File = file("./")): String {
    val byteOut = ByteArrayOutputStream()
    project.exec {
        workingDir = currentWorkingDir
        commandLine = this@runCommand.split("\\s".toRegex())
        standardOutput = byteOut
    }
    return String(byteOut.toByteArray()).trim()
}

val gitCommitHash = "git rev-parse --verify --short HEAD".runCommand()
val gitCommitCount = "git rev-list --count HEAD".runCommand().toInt()

android {
    namespace = "de.appsonair.wtf.osdplayer"
    compileSdk = 34
    buildToolsVersion = "34.0.0"

    defaultConfig {
        applicationId = "de.appsonair.wtf.osdplayer"
        minSdk = 24
        targetSdk = 34
        versionCode = 24
        versionName = "0.4"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField("String", "GIT_HASH", """"$gitCommitHash"""")
        buildConfigField("String", "MSP_OSD_VERSION", """"0.7.1"""")
        val mapboxToken = System.getenv("MAPBOX_TOKEN") ?:
        Properties().let { properties ->
            try {
                properties.load(rootProject.file("local.properties").bufferedReader())
                properties.getProperty("MAPBOX_TOKEN")
            } catch (err: Throwable) {
                "NO_TOKEN"
            }
        }
        buildConfigField("String", "MAPBOX_TOKEN", """"$mapboxToken"""")
    }

    lint {
        disable.addAll(listOf("GradleDependency","NotificationPermission", "ObsoleteLintCustomCheck"))
        abortOnError = true
        ignoreWarnings = false
        warningsAsErrors = true
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file("wtfdebug.keystore")
            storePassword = "android"
            keyAlias = "androiddebugkey"
            keyPassword = "android"
        }
        create("release") {
            storeFile = file("../local_data/uploadkeystore.jks")
            storePassword = System.getenv("UPLOAD_KEYSTORE_PW")
            keyAlias = "wtfosd"
            keyPassword = System.getenv("UPLOAD_KEYSTORE_PW")
        }
    }

    buildTypes {
        debug {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("debug")
        }
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = composeCompilerVersion
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
            excludes += "META-INF/LICENSE*"
        }
    }
}


java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

dependencies {

    implementation("com.halilibo.compose-richtext:richtext-commonmark:0.20.0")
    implementation("com.halilibo.compose-richtext:richtext-ui-material3:0.20.0")

    implementation("androidx.media3:media3-exoplayer:1.3.0")

    val coil = "2.6.0"
    implementation("io.coil-kt:coil-base:$coil")
    implementation("io.coil-kt:coil-compose:$coil")
    implementation("io.coil-kt:coil-video:$coil")

    // mapbox
    val mapboxVersion = "10.16.0"
    implementation("com.mapbox.maps:android:$mapboxVersion")
    implementation("com.mapbox.plugin:maps-annotation:$mapboxVersion")

    val okioVersion = "3.8.0"
    implementation("com.squareup.okio:okio:$okioVersion")

    implementation("androidx.documentfile:documentfile:1.0.1")
    implementation("com.google.accompanist:accompanist-systemuicontroller:0.34.0")

    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.6.1")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")

    lintChecks("com.slack.lint.compose:compose-lint-checks:1.3.1") // https://slackhq.github.io/compose-lints
    implementation("androidx.activity:activity-compose:1.9.0-alpha03")

    implementation("de.drick.compose:edge-to-edge-preview:0.2.0") // Compose edge-to-edge preview

    val composeBom = platform("androidx.compose:compose-bom:2024.02.02")
    implementation(composeBom)
    androidTestImplementation(composeBom)
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-tooling-preview:")
    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.material:material-icons-extended:")
    debugImplementation("androidx.compose.ui:ui-tooling:")
    debugImplementation("androidx.compose.ui:ui-test-manifest:")

    implementation("io.mockk:mockk:1.13.10") // Used to mock viewmodels for previews

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
}

android.applicationVariants.all {
    tasks.create("deploy${buildType.name.capitalized()}") {
        group = "deploy"
        description = "Deploy bundle to play store"

        val buildTypeName = buildType.name
        val aabFile = File(layout.buildDirectory.get().asFile, "outputs/bundle/$buildTypeName/app-$buildTypeName.aab")
        val releaseFolder = File("release")
        val apiAccessFile = File("local_data/api-access.json")

        dependsOn("bundle${buildType.name.capitalized()}")

        doLast {
            println("Execute task: $name")
            println("Application id: $applicationId")
            println("Version: $versionName ($versionCode)")
            println("Aab file: $aabFile")
            val publishOnPlayStore = de.appsonair.android_build_tools.PublishOnPlayStore(
                packageName = applicationId,
                apiAccessJsonFile = apiAccessFile
            )
            val editResult = publishOnPlayStore.insert {
                val bundle = uploadBundle(aabFile)
                val versionCode = bundle.versionCode
                println("Apk uploaded: $bundle version $versionCode")
                //mapping files
                //uploadDeobfuscationFiles(File("release/mapping.txt"), versionCode)
                val track = trackRelease(
                    versionCode = bundle.versionCode,
                    releaseName = "$versionName ($versionCode)",
                    releaseNotesUs = System.getenv("CI_COMMIT_TAG_MESSAGE")
                )
                println("Update track: $track")
            }
            println("App edit has committed: ${editResult.id}")

            publishOnPlayStore.downloadGeneratedUniversalApks(
                versionCode = versionCode,
                outputFolder = releaseFolder,
                fileName = "universal_${versionName}_${versionCode}"
            )

            releaseFolder.list().forEach {
                println("Created file: $it")
                println("Upload to")
            }
        }
    }
}