# WTF OSD Player
Please note that this project is under heavy development.
Since the FPV.WTF team rooted the DJI googles the project: https://github.com/fpv-wtf/msp-osd brings the full betaflight OSD to digital video.
It is also possible to record the osd data onto the googles.
You than have 3 different files: DJIG0003.mp4, DJIG0003.srt and DJIG0003.osd
The ...osd file contains the OSD character data.

## Enable osd recording
Follow the instructions on (https://github.com/fpv-wtf/msp-osd) to install msp-osd.
Than connect your google and input following in the wtfos configurator cli:
```
$ package-config set msp-osd rec_enabled true
$ package-config apply msp-osd
```
And than do some flight.

## This app

<p align="center">
    <img src="screenshot_1.webp" width="240px">
</p>

With this app you can open recorded video/osd files on you Android device and play the video back with OSD.
It is based on the msp-osd code (https://github.com/fpv-wtf/msp-osd) and the osd-dump-tools (https://github.com/Knifa/osd-dump-tools).
Currently it is only tested with betaflight and iNav osd data.

## Connect Android device to googles

Maybe you need a so called usb-c otg cable. It depends on your device model. Some can also connect with a normal usb-c to usb-c cabel with the googles.
In theory you get an additinal external storage device in you Android system.
In the app you select "Add new folder" and select the external storage device (pigeon)
Than you can open this folder in the app and play back all recorded flights.


