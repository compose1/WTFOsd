package de.appsonair.tools

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.DocumentsContract
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


data class WTFFile(
    val id: String,
    val uri: Uri,
    val name: String,
    val extension: String,
    val isDirectory: Boolean,
    val size: Long
)

/**
 * Unfortunately the official DocumentFile implementation of androidx is too slow.
 * So we need to resolve the uri requests manually here :-(
 */
class DirectoryResolver(private val ctx: Context) {

    private val projection = arrayOf(
        DocumentsContract.Document.COLUMN_DOCUMENT_ID,
        DocumentsContract.Document.COLUMN_DISPLAY_NAME,
        DocumentsContract.Document.COLUMN_SIZE,
        DocumentsContract.Document.COLUMN_MIME_TYPE,
        //DocumentsContract.Document.COLUMN_LAST_MODIFIED,
        //DocumentsContract.Document.COLUMN_FLAGS
    )

    private fun extractData(parentUri: Uri, cursor: Cursor): WTFFile {
        val id: String = cursor.getString(0)
        val uri = DocumentsContract.buildDocumentUriUsingTree(parentUri, id)
        val name: String = cursor.getString(1)
        val extension = name.substringAfterLast('.', "")
        val size: Long = cursor.getLong(2)
        val mimeType: String = cursor.getString(3)
        //val lastModified: Long = cursor.getLong(4)
        //val flags: Int = cursor.getLong(5).toInt()
        return WTFFile(
            id = id,
            uri = uri,
            name = name,
            extension = extension,
            isDirectory = mimeType == DocumentsContract.Document.MIME_TYPE_DIR,
            size = size
        )
    }

    fun getRoot(uri: Uri): WTFFile? {
        val id = DocumentsContract.getTreeDocumentId(uri)
        val treeUri = DocumentsContract.buildDocumentUriUsingTree(uri, id)
        ctx.contentResolver.query(treeUri, projection, null, null, null)?.use { cursor -> // Sort by file name
            if(cursor.moveToFirst()) {
                val file = extractData(uri, cursor)
                log("File: $file")
                return file
            }
        }
        return null
    }

    fun getChildren(uri: Uri): Flow<WTFFile> = flow {
        val isDocument = DocumentsContract.isDocumentUri(ctx, uri)
        val id = if (isDocument) DocumentsContract.getDocumentId(uri) else DocumentsContract.getTreeDocumentId(uri)
        val documentUri = DocumentsContract.buildDocumentUriUsingTree(uri, id)
        val listChildrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(documentUri, id)
        log("Query uri: $listChildrenUri")
        ctx.contentResolver.query(listChildrenUri, projection, null, null, null)?.use { cursor -> // Sort by file name
            while(cursor.moveToNext()) {
                val file = extractData(uri, cursor)
                log("File: $file")
                emit(file)
            }
        }
    }
}