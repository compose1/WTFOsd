package de.appsonair.wtf.osdplayer.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalInspectionMode
import de.appsonair.wtf.osdplayer.BuildConfig
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSDTheme
import de.drick.compose.edgetoedgepreviewlib.EdgeToEdgeTemplate
import de.drick.compose.edgetoedgepreviewlib.NavigationMode

@Composable
fun isPreviewMode() = BuildConfig.DEBUG && LocalInspectionMode.current

@Composable
fun PreviewTemplateRoot(
    modifier: Modifier = Modifier,
    showSystemUi: Boolean = true,
    content: @Composable () -> Unit
) {
    if (BuildConfig.DEBUG) {
        if (showSystemUi) {
            EdgeToEdgeTemplate(
                navMode = NavigationMode.Gesture,
                showInsetsBorder = false
            ) {
                WTFOSDTheme {
                    CompositionLocalProvider(
                        LocalContentColor provides MaterialTheme.colorScheme.onBackground
                    ) {
                        content()
                    }
                }
            }
        } else {
            WTFOSDTheme {
                Box(
                    modifier = modifier.background(MaterialTheme.colorScheme.background),
                    content = {
                        CompositionLocalProvider(
                            LocalContentColor provides MaterialTheme.colorScheme.onBackground
                        ) {
                            content()
                        }
                    }
                )
            }
        }
    }
}
