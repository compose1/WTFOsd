package de.appsonair.wtf.osdplayer.gps

import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.OsdRecord

data class GpsData(
    val wayPoints: List<GpsRecord>
)

data class GpsRecord(
    val position: GeoPoint,
    val osdMillis: Long
)

fun extractGps(osdRecord: OsdRecord): GpsData {
    val positionList = mutableListOf<GpsRecord>()
    osdRecord.frames.forEach { frame ->
        val latChars = frame.data.detectTrailingChars(137, 10)
        val lonChars = frame.data.detectTrailingChars(152, 10)
        if (latChars != null && lonChars != null) {
            val lat = latChars.cleanString().toDouble()
            val lon = lonChars.cleanString().toDouble()
            positionList.add(GpsRecord(GeoPoint(lat, lon), frame.osdMillis()))
        }
    }
    log("Gps data loaded ${positionList.size} points.")
    return GpsData(positionList)
}

fun ShortArray.detectTrailingChars(code: Int, number: Int): CharArray? {
    val shortCode = code.toShort()
    val index = indexOfFirst { it == shortCode }
    if (index >= 0) {
        val x = index / 22
        val y = index % 22
        return CharArray(number) {
            val p = x + it + 1
            val short = this[y + p * 22]
            Char(short.toInt())
        }
    } else {
        return null
    }
}

fun ShortArray.detectLeadingChars(code: Int, number: Int): CharArray? {
    val shortCode = code.toShort()
    val index = indexOfFirst { it == shortCode }
    return if (index >= 0) {
        val x = index / 22
        val y = index % 22
        CharArray(number) {
            val p = x - number + it
            val short = this[y + p * 22]
            Char(short.toInt())
        }
    } else {
        null
    }
}

fun CharArray.cleanString(): String {
    val endIndex = indexOf('\u0000')
    return if (endIndex >= 0) {
        concatToString(startIndex = 0, endIndex)
    } else {
        concatToString()
    }
}