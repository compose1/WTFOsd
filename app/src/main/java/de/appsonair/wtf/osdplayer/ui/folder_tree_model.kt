package de.appsonair.wtf.osdplayer.ui

import android.content.Context
import android.content.UriPermission
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import de.appsonair.tools.DirectoryResolver
import de.appsonair.tools.WTFFile
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.FontVariant
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.ui.compose.ProgressOverlayState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.toList

sealed interface FileItem {
    data object AddFolder: FileItem

    data class Folder(
        val name: String,
        var list: () -> List<FileItem>,
        var parent: Folder?
    ): FileItem

    data class UpFolder(
        val folder: Folder
    ): FileItem

    data class VideoFile(
        val name: String,
        val videoFile: WTFFile,
        val osdFile: WTFFile?,
        val srtFile: WTFFile?
    ): FileItem

    data class FontFile(
        val name: String,
        val variant: FontVariant,
        val file1: WTFFile,
        val file2: WTFFile?
    ): FileItem
}

fun getRootFolder(ctx: Context): FileItem.Folder {
    val folder = FileItem.Folder(
        name = "root",
        list = { emptyList() },
        parent = null
    )
    val dr = DirectoryResolver(ctx)
    folder.list = {
        val newList = ctx.contentResolver.persistedUriPermissions
            .mapNotNull { uriPermission: UriPermission ->
                val uri = uriPermission.uri
                log("Test new flow for: ${dr.getRoot(uri)}")
                val document = dr.getRoot(uri)
                document?.toFolder(ctx, folder)
            }.toMutableList<FileItem>()
        newList.add(FileItem.AddFolder)
        newList
    }
    return folder
}

fun WTFFile.toFolder(ctx: Context, parent: FileItem.Folder?): FileItem.Folder  {
    val dr = DirectoryResolver(ctx)
    val folder = FileItem.Folder(name, { emptyList() }, parent)
    folder.list = {
        val fullList = runBlocking { dr.getChildren(uri).toList() }
        val folderList = fullList.filter {
            it.isDirectory
        }
            .mapNotNull { document ->
                FileItem.Folder(document.name, document.toFolder(ctx, folder).list, folder)
            }
        val fontFiles = FontVariant.values().mapNotNull { variant ->
            val f1 = fullList.find { it.name == variant.fileName1() }
            val f2 = fullList.find { it.name == variant.fileName2() }
            if (f1 != null) {
                FileItem.FontFile(
                    name = ctx.getString(R.string.screen_font_title_template, variant.name),
                    variant = variant,
                    file1 = f1,
                    file2 = f2
                )
            } else {
                null
            }
        }
        val mp4FileList: List<WTFFile> = fullList
            .filter { it.isDirectory.not() }
            .filter { it.name.endsWith(".mp4") || it.name.endsWith(".mov") }
        val osdFileMap: Map<String, WTFFile> = fullList
            .filter { it.isDirectory.not() }
            .filter { it.name.endsWith(".osd") }
            .associateBy { it.name }
        //TODO srt file map
        val videoList = mp4FileList.map {
            val name = it.name.substring(0, it.name.lastIndexOf("."))
            val osdFileName = "$name.osd"
            FileItem.VideoFile(
                name = name,
                videoFile = it,
                osdFile = osdFileMap[osdFileName],
                srtFile = null
            )
        }
        folderList + fontFiles + videoList
    }
    return folder
}


@Composable
fun rememberFolderTreeState(): FolderTreeState {
    val scope = rememberCoroutineScope()
    val ctx = LocalContext.current
    return remember { FolderTreeState(ctx, scope, getRootFolder(ctx)) }
}

class FolderTreeState(
    private val ctx: Context,
    private val scope: CoroutineScope,
    folder: FileItem.Folder
) {
    val gridState = LazyGridState(0, 0)
    var currentFolder by mutableStateOf(folder)
        private set
    var fileList: List<FileItem> by mutableStateOf(emptyList())
        private set

    val progressState = ProgressOverlayState()

    init {
        switchFolder(folder)
    }

    fun switchFolder(folder: FileItem.Folder) {
        //TODO cancel running jobs
        progressState.cancelAndEnqueue {
            val list = withContext(Dispatchers.IO) { folder.list() }
            val parent = folder.parent
            currentFolder = folder
            fileList = if (parent != null) {
                listOf(FileItem.UpFolder(parent)) + list
            } else {
                list
            }
        }
    }

    fun back() {
        currentFolder.parent?.let { parent ->
            switchFolder(parent)
        }
    }
}
