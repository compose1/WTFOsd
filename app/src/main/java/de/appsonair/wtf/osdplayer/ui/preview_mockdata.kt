package de.appsonair.wtf.osdplayer.ui

import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.runtime.Composable
import de.appsonair.tools.WTFFile
import de.appsonair.wtf.osdplayer.FontVariant
import de.appsonair.wtf.osdplayer.ui.compose.ProgressOverlayState
import de.appsonair.wtf.osdplayer.ui.compose.VideoPlayerState
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot

@Composable
fun mockMainViewModel(
    mainUiState: MainUiState,
    isLoading: Boolean = false,
    folderTreeState: FolderTreeState = mockFolderTreeState(),
    isFontActive: Boolean = false
): MainViewModel {
    val vmMock = mockk<MainViewModel>()
    every { vmMock.mainUiState } returns mainUiState
    every { vmMock.isLoading } returns isLoading
    every { vmMock.folderTreeState } returns folderTreeState
    val slot = slot<FileItem.FontFile>()
    every { vmMock.isActive(capture(slot)) } returns isFontActive
    return vmMock
}

fun mockFolder(name: String) = FileItem.Folder(name, { emptyList() }, null)
val mockUpFolder = FileItem.UpFolder(FileItem.Folder("root", { emptyList() }, null))
fun mockFontFolder(name: String, variant: FontVariant = FontVariant.BETAFLIGHT): FileItem.FontFile {
    val file1 = WTFFile(
        id = name,
        uri = Uri.parse("file:///android_asset/${variant.fileName1()}"),
        name = name,
        extension = "bf",
        isDirectory = false,
        size = 0
    )
    val file2 = WTFFile(
        id = name,
        uri = Uri.parse("file:///android_asset/${variant.fileName2()}"),
        name = name,
        extension = "bf",
        isDirectory = false,
        size = 0
    )
    return FileItem.FontFile(
        name = name,
        variant = variant,
        file1 = file1,
        file2 = file2
    )
}
fun mockVideo(
    name: String,
    @DrawableRes resId: Int,
    osd: Boolean = true
): FileItem.VideoFile {
    return FileItem.VideoFile(
        name = name,
        WTFFile(
            id = resId.toString(),
            uri = Uri.EMPTY,
            name = name,
            extension = "mp4",
            isDirectory = false,
            size = 0L
        ),
        osdFile = if (osd) WTFFile(
            id = resId.toString(),
            uri = Uri.EMPTY,
            name = name,
            extension = "osd",
            isDirectory = false,
            size = 0L
        ) else null,
        srtFile = null
    )
}

@Composable
fun mockFolderTreeState(
    vararg file: FileItem
): FolderTreeState {
    val mock = mockk<FolderTreeState>()
    val folder: FileItem.Folder = FileItem.Folder(
        name = "root",
        list = { if (file.isEmpty()) listOf(FileItem.AddFolder) else file.toList() },
        parent = null
    )
    val gridState = rememberLazyGridState()
    val progressState = ProgressOverlayState()
    every { mock.currentFolder } returns folder
    every { mock.gridState } returns gridState
    every { mock.progressState } returns progressState
    every { mock.fileList } returns folder.list()
    return mock
}

fun mockVideoState(
    aspectRatio: Float = 1.3f
): VideoPlayerState {
    val state = mockk<VideoPlayerState>()
    val slot = slot<FileItem.VideoFile>()
    every { state.play(capture(slot)) } returns Unit
    every { state.aspectRatio } returns aspectRatio
    every { state.state } returns VideoPlayerState.PlayBackState.PLAY
    return state
}
