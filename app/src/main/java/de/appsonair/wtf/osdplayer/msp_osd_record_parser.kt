package de.appsonair.wtf.osdplayer

import de.appsonair.tools.log
import de.appsonair.tools.logE
import java.io.InputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.math.roundToLong

data class OsdRecord(
    val header: String,
    val version: Int,
    val charWidth: Int,
    val charHeight: Int,
    val fontWidth: Int,
    val fontHeight: Int,
    val fontVariant: Int,
    val xOffset: Int,
    val yOffset: Int,
    val frames: List<Frame>
)

class Frame(
    val idx: Int,
    val data: ShortArray
) {
    fun osdMillis(): Long = (idx.toFloat() * (1000f/60f)).roundToLong()
}

/**
 *
typedef struct rec_file_header_t
{
char magic[7];
uint16_t version;
rec_config_t config;
} __attribute__((packed)) rec_file_header_t;

typedef struct rec_config_t
{
uint8_t char_width;
uint8_t char_height;
uint8_t font_width;
uint8_t font_height;
uint16_t x_offset;
uint16_t y_offset;
uint8_t font_variant;
} __attribute__((packed)) rec_config_t;


typedef struct rec_frame_header_t
{
uint32_t frame_idx;
uint32_t size;
} __attribute__((packed)) rec_frame_header_t;

 */
fun parseOsdFile(stream: InputStream) : OsdRecord {
    val byteBuffer = ByteBuffer.wrap(stream.readBytes()).order(ByteOrder.LITTLE_ENDIAN)
    //Read header
    val header = (0..6).map { Char(byteBuffer.get().toInt()) }.joinToString("")
    val version = byteBuffer.short.toUShort()
    val charWidth = byteBuffer.get().toInt()
    val charHeight = byteBuffer.get().toInt()
    val fontWidth = byteBuffer.get().toInt()
    val fontHeight = byteBuffer.get().toInt()
    val xOffset = byteBuffer.short.toUShort().toInt()
    val yOffset = byteBuffer.short.toUShort().toInt()
    val fontVariant = byteBuffer.get().toInt()

    val expectedHeader = "MSPOSD\u0000"
    log("file header : $header")
    assert(expectedHeader == header) { "File header is not as expected!" }
    log("file version: $version")
    log("char width  : $charWidth")
    log("char height : $charHeight")
    log("font width  : $fontWidth")
    log("font height : $fontHeight")
    log("x offset    : $xOffset")
    log("y offset    : $yOffset")
    log("font variant: $fontVariant")
    val frames = mutableListOf<Frame>()
    var isFirstFrame = true
    while (byteBuffer.remaining() > 8) {
        var frameIdx = byteBuffer.int
        if (isFirstFrame && frameIdx > 100) frameIdx = 1
        isFirstFrame = false
        val frameSize = byteBuffer.int
        // Check frame size

        val maxY = charHeight - 1
        val maxX = charWidth - 1
        val expectedSize = maxY + maxX * 22
        if (frameSize < expectedSize) {
            logE("Frame size: $frameSize expected: $expectedSize")
            break
        }

        val frameByteSize = frameSize * 2
        if (byteBuffer.remaining() >= frameByteSize) {
            val frameData = ShortArray(frameSize)
            for (i in 0 until frameSize) {
                frameData[i] = byteBuffer.short
            }
            frames.add(Frame(frameIdx, frameData))
        }
    }
    println("Frames: ${frames.size}")
    return OsdRecord(
        header = header,
        version = version.toInt(),
        charWidth = charWidth,
        charHeight = charHeight,
        fontWidth = fontWidth,
        fontHeight = fontHeight,
        fontVariant = fontVariant,
        xOffset = xOffset,
        yOffset = yOffset,
        frames = frames
    )
}