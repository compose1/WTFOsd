package de.appsonair.wtf.osdplayer.ui

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.appsonair.wtf.osdplayer.BuildConfig
import de.appsonair.wtf.osdplayer.R

@Preview(
    name = "Portrait",
    device = "spec:width=411dp,height=891dp",
    uiMode = Configuration.UI_MODE_NIGHT_NO
)
@Preview(
    name = "Portrait night",
    device = "spec:width=411dp,height=891dp",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
@Preview(
    name = "Landscape",
    device = "spec:width=891dp,height=411dp",
    fontScale = 1.5f
)
@Preview(
    name = "Landscape night",
    device = "spec:width=891dp,height=411dp",
    uiMode = Configuration.UI_MODE_NIGHT_YES
)
annotation class ScreenSizesPreviews


@ScreenSizesPreviews
@Composable
private fun PreviewAbout() {
    PreviewTemplateRoot {
        AboutScreen(
            modifier = Modifier.fillMaxSize(),
            onUrlClicked = {},
            onStart = {}
        )
    }
}

@Composable
fun AboutScreen(
    onUrlClicked: (String) -> Unit,
    onStart: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .background(MaterialTheme.colorScheme.background)
            .safeDrawingPadding()
            .fillMaxSize()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            Text(
                text = stringResource(R.string.app_name),
                style = MaterialTheme.typography.titleMedium
            )
            Text("${stringResource(R.string.about_version)} ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})")
            Text("${stringResource(R.string.about_git_hash)} ${BuildConfig.GIT_HASH}")
            Text("${stringResource(R.string.about_msp_version)} ${BuildConfig.MSP_OSD_VERSION}")
            Spacer(Modifier.height(8.dp))
            Text(text = stringResource(R.string.about_this_project))
            val sourceUrl = "https://gitlab.com/timod/WTFOsd"
            Text(
                modifier = Modifier.clickable(onClick = { onUrlClicked(sourceUrl) }),
                text = sourceUrl,
                color = MaterialTheme.colorScheme.secondary
            )
            Text(stringResource(R.string.about_based_on))
            val mspOsdUrl = "https://github.com/fpv-wtf/msp-osd"
            Text(
                modifier = Modifier.clickable(onClick = { onUrlClicked(mspOsdUrl) }),
                text = mspOsdUrl,
                color = MaterialTheme.colorScheme.secondary
            )
        }
        ExtendedFloatingActionButton(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(bottom = 16.dp),
            onClick = onStart
        ) {
            Text(stringResource(R.string.about_start))
        }
    }
}