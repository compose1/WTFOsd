package de.appsonair.wtf.osdplayer.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Map
import androidx.compose.material.icons.filled.ZoomIn
import androidx.compose.material.icons.filled.ZoomOut
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.ui.theme.ColorsImageOverlay

@VideoScreenSizesPreviews
@Composable
private fun PreviewOverlay16_9GPS() {
    PreviewOsdPlayerScaffold(
        aspect = 1.77778f,
        gps = true,
        overlay = {
            OsdPlayerOverlay(
                modifier = Modifier.fillMaxSize(),
                controlButtonState = OverlayControlButtonState.PLAY,
                osdState = OverlayButtonState.ACTIVE,
                gpsState = OverlayButtonState.ACTIVE,
                progress = 0.5f,
                onAction = {},
                onSeek = {}
            )
        }
    )
}

enum class OverlayAction {
    PLAY_PAUSE_TOGGLE,
    OSD_TOGGLE,
    GPS_TOGGLE,
    GPS_INFO,
    GPS_ZOOM_IN,
    GPS_ZOOM_OUT,
    
}

@Composable
fun OsdPlayerOverlay(
    controlButtonState: OverlayControlButtonState,
    osdState: OverlayButtonState,
    gpsState: OverlayButtonState,
    progress: Float,
    onAction: (OverlayAction) -> Unit,
    onSeek: (Float) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier) {
        OverlayActionButton(
            modifier = Modifier.align(Alignment.Center),
            icon = controlButtonState.icon,
            contentDescription = stringResource(R.string.screen_osd_player_play_pause),
            onClick = { onAction(OverlayAction.PLAY_PAUSE_TOGGLE) }
        )
        Column(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(horizontal = 36.dp, vertical = 24.dp),
            verticalArrangement = Arrangement.spacedBy(24.dp),
        ) {
            OverlayStateButton(
                modifier = Modifier
                    .clickable(onClick = { onAction(OverlayAction.OSD_TOGGLE) })
                    .size(64.dp),
                state = osdState
            ) {
                Text(
                    modifier = Modifier.padding(2.dp),
                    text = stringResource(R.string.screen_osd_player_osd),
                    fontSize = 22.sp
                )
            }
            Row(
                horizontalArrangement = Arrangement.spacedBy(16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                OverlayStateButton(
                    modifier = Modifier
                        .clickable(onClick = { onAction(OverlayAction.GPS_TOGGLE) })
                        .size(64.dp)
                    ,
                    state = gpsState,
                ) {
                    Icon(
                        imageVector = Icons.Filled.Map,
                        contentDescription = stringResource(id = R.string.screen_osd_player_gps)
                    )
                }
                if (gpsState == OverlayButtonState.ACTIVE) {
                    OverlayActionButton(
                        icon = Icons.Filled.Info,
                        contentDescription = stringResource(id = R.string.screen_osd_player_gps_info),
                        onClick = { onAction(OverlayAction.GPS_INFO) }
                    )
                    OverlayActionButton(
                        icon = Icons.Filled.ZoomOut,
                        contentDescription = stringResource(id = R.string.screen_osd_player_gps_zoom_out),
                        onClick = { onAction(OverlayAction.GPS_ZOOM_OUT) }
                    )
                    OverlayActionButton(
                        icon = Icons.Filled.ZoomIn,
                        contentDescription = stringResource(id = R.string.screen_osd_player_gps_zoom_in),
                        onClick = { onAction(OverlayAction.GPS_ZOOM_IN) }
                    )
                }
            }
            Slider(
                modifier = Modifier.fillMaxWidth(),
                colors = SliderDefaults.colors(
                    thumbColor = ColorsImageOverlay.text(),
                    activeTrackColor = ColorsImageOverlay.text()
                ),
                value = progress,
                onValueChange = onSeek
            )
        }
    }
}

@Composable
fun OverlayActionButton(
    icon: ImageVector,
    contentDescription: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    inverted: Boolean = false
) {
    val foregroundColor = if (inverted.not()) ColorsImageOverlay.text() else ColorsImageOverlay.background()
    val backgroundColor = if (inverted.not()) ColorsImageOverlay.background() else ColorsImageOverlay.text()
    IconButton(
        modifier = modifier
            .size(64.dp)
            .background(
                color = backgroundColor,
                shape = CircleShape
            ),
        onClick = onClick,
    ) {
        Icon(
            modifier = Modifier.size(48.dp),
            imageVector = icon,
            contentDescription = contentDescription,
            tint = foregroundColor
        )
    }
}

@Composable
fun OverlayStateButton(
    state: OverlayButtonState,
    modifier: Modifier = Modifier,
    icon: @Composable () -> Unit
) {
    val shape = RoundedCornerShape(4.dp)
    val active = state == OverlayButtonState.ACTIVE
    val bgColor = if (active) ColorsImageOverlay.text() else ColorsImageOverlay.background()
    val contentColor = if (active) ColorsImageOverlay.shadow() else ColorsImageOverlay.text()
    if (state != OverlayButtonState.DISABLED) {
        Box(
            modifier = modifier
                .border(width = 1.dp, color = ColorsImageOverlay.text(), shape = shape)
                .background(color = bgColor, shape = shape)
                .padding(4.dp),
            contentAlignment = Alignment.Center
        ) {
            CompositionLocalProvider(LocalContentColor provides contentColor) {
                icon()
            }
        }
    }
}
