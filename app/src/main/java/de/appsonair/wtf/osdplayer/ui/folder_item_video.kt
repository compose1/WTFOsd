package de.appsonair.wtf.osdplayer.ui

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material3.LocalContentColor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.drawscope.inset
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.ui.compose.forwardingPainter
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSTypes

@Preview(widthDp = 200, heightDp = 150)
@Preview(widthDp = 200, heightDp = 150,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
private fun PreviewVideoItem() {
    PreviewTemplateRoot(showSystemUi = false) {
        VideoItemView(
            modifier = Modifier.padding(8.dp),
            item = mockVideo("Test", R.drawable.video_thumb1),
            onClick = {}
        )
    }
}
@Preview(widthDp = 200, heightDp = 150)
@Preview(widthDp = 200, heightDp = 150,
    uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL
)
@Composable
private fun PreviewVideoItemPlaceholder() {
    PreviewTemplateRoot(showSystemUi = false) {
        VideoItemView(
            modifier = Modifier.padding(8.dp),
            item = mockVideo("Test", 0),
            onClick = {}
        )
    }
}

@Composable
fun VideoItemView(
    item: FileItem.VideoFile,
    onClick: (FileItem.VideoFile) -> Unit,
    modifier: Modifier = Modifier,
) {
    Box(
        modifier = modifier
            .clip(RoundedCornerShape(8.dp))
            .clickable(onClick = { onClick(item) })
    ) {
        val previewMode = isPreviewMode()
        val contentColor = LocalContentColor.current
        val placeholder = rememberVectorPainter(Icons.Default.Movie)
        val tintedPlaceholder = if (previewMode && item.videoFile.id != "0") {
            val resId = item.videoFile.id.toInt()
            painterResource(id = resId)
        } else {
            remember {
                forwardingPainter(
                    painter = placeholder,
                    colorFilter = ColorFilter.tint(contentColor)
                ) { info ->
                    inset(-50f) {
                        with(info.painter) {
                            draw(size, info.alpha, info.colorFilter)
                        }
                    }
                }
            }
        }
        AsyncImage(
            modifier = Modifier.fillMaxSize(),
            model = item.videoFile.uri,
            contentDescription = null,
            placeholder = tintedPlaceholder,
            contentScale = ContentScale.Crop
        )
        if (item.osdFile != null) {
            OverlayStateButton(
                modifier = Modifier
                    .align(Alignment.TopEnd)
                    .padding(8.dp),
                state = OverlayButtonState.INACTIVE
            ) {
                Text(stringResource(R.string.screen_osd_player_osd))
            }
        }
        val backgroundInverted = remember {
            Brush.verticalGradient(
                0f to Color.Transparent,
                0.5f to Color.Black.copy(alpha = 0.4f),
            )
        }
        Spacer(
            Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .height(30.dp)
                .background(backgroundInverted))
        Text(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(8.dp),
            text = item.name,
            style = WTFOSTypes.thumbnailTitle()
        )
    }
}