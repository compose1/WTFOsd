package de.appsonair.wtf.osdplayer.ui

import android.content.res.Configuration
import androidx.activity.compose.BackHandler
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.FontRepository
import de.appsonair.wtf.osdplayer.FontVariant
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.ui.compose.ProgressOverlay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@Retention(AnnotationRetention.BINARY)
@Target(
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.FUNCTION
)
@Preview(name = "Light", group = "Light")
@Preview(name = "Dark", group = "Dark", uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL)
@Preview(name = "085%", group = "Font", fontScale = 0.85f)
@Preview(name = "100%", group = "Font", fontScale = 1.0f)
@Preview(name = "150%", group = "Font", fontScale = 1.5f)
@Preview(name = "200%", group = "Font", fontScale = 2f)
annotation class MainPreviews


@MainPreviews
@Composable
private fun MainLoadingPreview() {
    PreviewTemplateRoot {
        val vmMock = mockMainViewModel(
            mainUiState = MainUiState.About,
            isLoading = true
        )
        MainScreen(onOpenUrl = {}, onExit = { /*TODO*/ }, vm = vmMock)
    }
}

@MainPreviews
@Composable
private fun MainAboutPreview() {
    PreviewTemplateRoot {
        val viewModel = mockMainViewModel(
            mainUiState = MainUiState.About
        )
        MainScreen(onOpenUrl = {}, onExit = { /*TODO*/ }, vm = viewModel)
    }
}

@MainPreviews
@Composable
private fun MainFolderPreview() {
    PreviewTemplateRoot {
        val viewModel = mockMainViewModel(
            mainUiState = MainUiState.ViewFolder,
            folderTreeState = mockFolderTreeState()
        )
        MainScreen(onOpenUrl = {}, onExit = { /*TODO*/ }, vm = viewModel)
    }
}
@MainPreviews
@Composable
private fun MainFolderPreview2() {
    PreviewTemplateRoot {
        val folderState = mockFolderTreeState(
            mockUpFolder,
            mockFolder("fonts"),
            mockFolder("videos"),
            mockVideo("FLight 1", R.drawable.video_thumb1),
            mockVideo("FLight 2", R.drawable.video_thumb2),
            mockVideo("FLight 3", R.drawable.video_thumb3),
            mockVideo("FLight 4", R.drawable.video_thumb4),
        )
        val viewModel = mockMainViewModel(
            mainUiState = MainUiState.ViewFolder,
            folderTreeState = folderState
        )
        MainScreen(onOpenUrl = {}, onExit = { /*TODO*/ }, vm = viewModel)
    }
}

@MainPreviews
@Composable
private fun MainFontPreview() {
    PreviewTemplateRoot {
        val fontRepository = FontRepository(LocalContext.current)
        val font = runBlocking {
            fontRepository.loadFont(FontVariant.BETAFLIGHT)
        }
        val fontFolder = mockFontFolder("test")
        val viewModel = mockMainViewModel(
            mainUiState = MainUiState.ViewFont(fontFolder, font, font.extractLogo()),
            folderTreeState = mockFolderTreeState()
        )
        MainScreen(onOpenUrl = {}, onExit = { /*TODO*/ }, vm = viewModel)
    }
}

@Composable
fun MainScreen(
    onOpenUrl: (String) -> Unit,
    onExit: () -> Unit,
    modifier: Modifier = Modifier,
    vm: MainViewModel = viewModel(),
) {
    val scope = rememberCoroutineScope()
    BackHandler {
        if (vm.back().not()) {
            onExit()
        }
    }
    CompositionLocalProvider(
        LocalContentColor provides MaterialTheme.colorScheme.onBackground
    ) {
        //val uri = videoFile.selectedUri
        when (val state = vm.mainUiState) {
            is MainUiState.About -> AboutScreen(
                onUrlClicked = onOpenUrl,
                onStart = { vm.start() }
            )

            is MainUiState.ViewFolder -> {
                FolderTreeScreen(
                    state = vm.folderTreeState,
                    onFileSelected = { vm.fileSelected(it) }
                )
            }

            is MainUiState.ViewFont -> {
                val file = state.file
                var isActive by remember { mutableStateOf(vm.isActive(file)) }
                FontScreen(
                    font = state.font,
                    logo = state.logo,
                    isActive = isActive,
                    onSelected = {
                        scope.launch {
                            vm.fontSelected(file)
                            isActive = vm.isActive(file)
                        }
                    }
                )
            }

            is MainUiState.ViewVideo -> {
                log("selected uri: ${state.file}")
                OsdPlayerScreen(videoFile = state.file)
            }
        }

        ProgressOverlay(isVisible = vm.isLoading)
    }
}