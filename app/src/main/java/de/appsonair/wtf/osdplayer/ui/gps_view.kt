package de.appsonair.wtf.osdplayer.ui

import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.graphics.Color.Companion.White
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.gps.GpsData
import de.appsonair.wtf.osdplayer.gps.GpsRecord
import de.appsonair.wtf.osdplayer.ui.compose.CircleStyle
import de.appsonair.wtf.osdplayer.ui.compose.MapBox
import de.appsonair.wtf.osdplayer.ui.compose.MapBoxState
import kotlinx.coroutines.isActive

private val styleCurrentPoint = CircleStyle(
    radius = 4.0,
    color = Green,
    strokeColor = White,
    strokeWidth = 1.0
)
private val styleStartPoint = CircleStyle(
    radius = 4.0,
    color = White,
    strokeColor = Black,
    strokeWidth = 1.0
)
private val styleEndPoint = CircleStyle(
    radius = 4.0,
    color = Black,
    strokeColor = White,
    strokeWidth = 1.0
)

@Composable
fun GpsView(
    mapBoxState: MapBoxState,
    gpsData: GpsData,
    positionProvider: () -> Long,
    modifier: Modifier = Modifier
) {
    var frame: GpsRecord by remember(gpsData) { mutableStateOf(gpsData.wayPoints.first()) }
    //val mapBoxState = rememberMapBoxState(frame.position)
    LaunchedEffect(gpsData) {
        log("launched effect start")
        mapBoxState.resetToDefault()
        val geoPoints = gpsData.wayPoints.map { it.position }
        if (geoPoints.size > 1) {
            mapBoxState.drawOverview(geoPoints)
            mapBoxState.drawPoint(geoPoints.first(), styleStartPoint)
            mapBoxState.drawPoint(geoPoints.last(), styleEndPoint)
        }
        val currentPositionPoint = mapBoxState.drawPoint(geoPoints.first(), styleCurrentPoint)
        mapBoxState.updateCamera(geoPoints.first(), 0.0, false)
        val frameIterator = gpsData.wayPoints.listIterator()
        var currentFrame = frameIterator.next()
        while (isActive) {
            val prevFrame = currentFrame
            withFrameMillis {
                val videoPositionMillis = positionProvider()
                val currentOsdMillis = currentFrame.osdMillis
                if (currentOsdMillis + 100 < videoPositionMillis) {
                    //Seek forward in osd frames
                    while (currentFrame.osdMillis < videoPositionMillis && frameIterator.hasNext()) {
                        currentFrame = frameIterator.next()
                    }
                } else if (currentOsdMillis - 100 > videoPositionMillis) {
                    //Seek backward in osd frames
                    while (currentFrame.osdMillis > videoPositionMillis && frameIterator.hasPrevious()) {
                        currentFrame = frameIterator.previous()
                    }
                }
                frame = currentFrame
            }
            if (currentFrame != prevFrame) {
                mapBoxState.updatePoint(currentFrame.position, currentPositionPoint)
                mapBoxState.updateCamera(currentFrame.position, 0.0)
            }
        }
        log("launched effect end")
    }
    MapBox(
        modifier = modifier,
        state = mapBoxState,
        onDisableCameraFocus = { /*TODO*/ }
    )
}
