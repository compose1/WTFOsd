package de.appsonair.wtf.osdplayer.ui

import android.app.Application
import android.view.MotionEvent
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Replay
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.Frame
import de.appsonair.wtf.osdplayer.OsdRecord
import de.appsonair.wtf.osdplayer.*
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.gps.GpsData
import de.appsonair.wtf.osdplayer.gps.extractGps
import de.appsonair.wtf.osdplayer.ui.compose.VideoPlayer
import de.appsonair.wtf.osdplayer.ui.compose.VideoPlayerState
import de.appsonair.wtf.osdplayer.parseOsdFile
import de.appsonair.wtf.osdplayer.ui.compose.LifecycleResumeEffect
import de.appsonair.wtf.osdplayer.ui.compose.rememberMapBoxState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Float.max
import kotlin.math.min
import kotlin.math.roundToLong

class OsdPlayerViewModel(private val ctx: Application) : AndroidViewModel(ctx) {
    private val fontRepository = FontRepository(ctx)
    val videoPlayerState = VideoPlayerState(ctx)
    var osdData: OsdData? by mutableStateOf(null)
        private set
    var gpsData: GpsData? by mutableStateOf(null)
        private set

    init {
        log("Create OSDPlayerViewModel")
    }

    suspend fun loadData(videoFile: FileItem.VideoFile) = withContext(Dispatchers.IO) {
        log("Load data for video: ${videoFile.videoFile.name}")
        val record = videoFile.osdFile?.let { file ->
            log("Load osd data: ${file.name}")
            ctx.contentResolver.openInputStream(file.uri).use { stream ->
                parseOsdFile(checkNotNull(stream))
            }
        }
        if (record != null) {
            val variant = FontVariant.fromVariant(record.fontVariant)
            val font = fontRepository.loadFont(variant)
            val data = OsdData(record, font)
            log("Set new osd data")
            osdData = data
            log("Load gps data...")
            val gpsTmp = extractGps(data.record)
            gpsData = if (gpsTmp.wayPoints.isNotEmpty()) gpsTmp else null
        } else {
            osdData = null
            gpsData = null
        }
    }

    override fun onCleared() {
        log("Cleanup OSDPlayerViewModel")
        videoPlayerState.stop()
        videoPlayerState.release()
    }
}

enum class OverlayControlButtonState(val icon: ImageVector) {
    PLAY(Icons.Default.PlayArrow),
    PAUSE(Icons.Default.Pause),
    REPLAY(Icons.Default.Replay)
}

enum class OverlayButtonState {
    ACTIVE, INACTIVE, DISABLED
}

@Composable
fun OsdPlayerScreen(videoFile: FileItem.VideoFile, vm: OsdPlayerViewModel = viewModel()) {
    LaunchedEffect(videoFile) {
        vm.loadData(videoFile)
    }
    OsdPlayerView(
        videoFile = videoFile,
        videoPlayerState = vm.videoPlayerState,
        osdData = vm.osdData,
        gpsData = vm.gpsData
    )
}

data class OsdData(
    val record: OsdRecord,
    val font: OsdFont
)

@Preview(device = "spec:parent=pixel_5,orientation=landscape")
@Composable
private fun PreviewOsdPlayer() {
    PreviewTemplateRoot(showSystemUi = false) {
        val state = mockVideoState(1.66f)
        val video = mockVideo("Video1", R.drawable.video_thumb1)
        OsdPlayerView(
            videoFile = video,
            videoPlayerState = state,
            osdData = null,
            gpsData = null
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class, ExperimentalLayoutApi::class)
@Composable
fun OsdPlayerView(
    videoFile: FileItem.VideoFile,
    videoPlayerState: VideoPlayerState,
    osdData: OsdData?,
    gpsData: GpsData?,
    modifier: Modifier = Modifier
) {
    val view = LocalView.current
    val scope = rememberCoroutineScope()

    var showControlOverlay by remember { mutableStateOf(false) }
    var showOsd by remember { mutableStateOf(true) }
    var lastTouchTs by remember { mutableLongStateOf(0L) }
    val systemUiController = rememberSystemUiController()
    var gpsButtonState by remember(gpsData) { mutableStateOf(if (gpsData != null) OverlayButtonState.ACTIVE else OverlayButtonState.DISABLED) }

    LaunchedEffect(showControlOverlay, lastTouchTs) {
        if (showControlOverlay) {
            delay(4000)
            showControlOverlay = false
        }
    }
    LaunchedEffect(videoPlayerState.state) {
        view.keepScreenOn = (videoPlayerState.state == VideoPlayerState.PlayBackState.PLAY)
        log("Keep screen on: ${view.keepScreenOn}")
    }

    LifecycleResumeEffect(videoFile) {
        systemUiController.isSystemBarsVisible = false
        systemUiController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        log("Resume started")
        videoPlayerState.play(videoFile)
        onPauseOrDispose {
            systemUiController.isSystemBarsVisible = true
            view.keepScreenOn = false
            log("Resume ended keep screen on: ${view.keepScreenOn}")
            videoPlayerState.stop()
        }
    }

    val controlButtonState by remember {
        derivedStateOf {
            when (videoPlayerState.state) {
                VideoPlayerState.PlayBackState.PLAY -> OverlayControlButtonState.PAUSE
                VideoPlayerState.PlayBackState.PAUSE -> OverlayControlButtonState.PLAY
                VideoPlayerState.PlayBackState.ENDED -> OverlayControlButtonState.REPLAY
            }
        }
    }
    var progress by remember { mutableFloatStateOf(0f) }
    fun calculateProgress(): Float =
        (videoPlayerState.position.toDouble() / videoPlayerState.duration.toDouble()).toFloat()

    LaunchedEffect(Unit) {
        while (true) {
            delay(500)
            progress = calculateProgress()
        }
    }
    val osdState by remember(osdData) {
        derivedStateOf {
            when {
                showOsd && osdData != null -> OverlayButtonState.ACTIVE
                showOsd.not() && osdData != null -> OverlayButtonState.INACTIVE
                else -> OverlayButtonState.DISABLED
            }
        }
    }
    val mapState = rememberMapBoxState()
    fun onTap() {
        log("Tap detected")
        lastTouchTs = System.currentTimeMillis()
        showControlOverlay = showControlOverlay.not()
    }
    OsdPlayerScaffold(
        modifier = modifier
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = {
                        onTap()
                    }
                )
            }
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    lastTouchTs = System.currentTimeMillis()
                    change.consume()
                    val newPosition =
                        videoPlayerState.position + (dragAmount.x * 20f).roundToLong()
                    videoPlayerState.seek(newPosition)
                }
            },
        videoAspectRatio = videoPlayerState.aspectRatio,
        showControlOverlay = showControlOverlay,
        showGps = gpsButtonState == OverlayButtonState.ACTIVE,
        videoPlayer = {
            VideoPlayer(
                modifier = Modifier.fillMaxSize(),
                state = videoPlayerState
            )
            if (osdData != null && osdState == OverlayButtonState.ACTIVE) {
                OsdCanvasView(
                    modifier = Modifier.fillMaxSize(),
                    osdRecord = osdData.record,
                    osdFont = osdData.font,
                    positionProvider = { videoPlayerState.position }
                )
            }
        },
        gpsMap = {
            if (gpsData != null) {
                GpsView(
                    modifier = Modifier
                        .pointerInteropFilter { event ->
                            when (event.action) {
                                MotionEvent.ACTION_UP -> {
                                    log("Map clicked")
                                    onTap()
                                    /*scope.launch {
                                        //state.showAttributionDialog(ctx)
                                        gpsFullView = true
                                    }*/
                                }
                            }
                            true
                        }
                    //.clip(CircleShape)
                    ,
                    gpsData = gpsData,
                    mapBoxState = mapState,
                    positionProvider = { videoPlayerState.position }
                )
            }
        },
        controlOverlay = {
            OsdPlayerOverlay(
                modifier = Modifier
                    .windowInsetsPadding(
                        WindowInsets.systemBarsIgnoringVisibility.union(WindowInsets.displayCutout)
                    )
                    .fillMaxSize(),
                controlButtonState = controlButtonState,
                progress = progress,
                osdState = osdState,
                gpsState = gpsButtonState,
                onAction = {
                    lastTouchTs = System.currentTimeMillis()
                    when (it) {
                        OverlayAction.PLAY_PAUSE_TOGGLE -> {
                            lastTouchTs = System.currentTimeMillis()
                            when (videoPlayerState.state) {
                                VideoPlayerState.PlayBackState.PLAY -> videoPlayerState.pause()
                                VideoPlayerState.PlayBackState.PAUSE -> videoPlayerState.play()
                                VideoPlayerState.PlayBackState.ENDED -> videoPlayerState.play(
                                    videoFile
                                )
                            }
                        }
                        OverlayAction.OSD_TOGGLE -> {
                            lastTouchTs = System.currentTimeMillis()
                            showOsd = showOsd.not()
                        }
                        OverlayAction.GPS_TOGGLE -> {
                            gpsButtonState = if (gpsButtonState == OverlayButtonState.ACTIVE)
                                OverlayButtonState.INACTIVE
                            else
                                OverlayButtonState.ACTIVE
                        }
                        OverlayAction.GPS_ZOOM_IN -> {
                            scope.launch {
                                mapState.zoomIn()
                            }
                        }
                        OverlayAction.GPS_ZOOM_OUT -> {
                            scope.launch {
                                mapState.zoomOut()
                            }
                        }
                        OverlayAction.GPS_INFO -> {
                            scope.launch {
                                mapState.showAttributionDialog(view.context)
                            }
                        }
                    }
                },
                onSeek = {
                    lastTouchTs = System.currentTimeMillis()
                    val newPosition = (videoPlayerState.duration * it).roundToLong()
                    videoPlayerState.seek(newPosition)
                    progress = calculateProgress()
                }
            )
        }
    )
}

@Composable
fun OsdCanvasView(
    osdRecord: OsdRecord,
    osdFont: OsdFont,
    positionProvider: () -> Long,
    modifier: Modifier = Modifier
) {
    var frame: Frame? by remember { mutableStateOf(null) }
    LaunchedEffect(osdRecord) {
        val frameIterator = osdRecord.frames.listIterator()
        var currentFrame = frameIterator.next()
        while (isActive) {
            withFrameMillis {
                val videoPositionMillis = positionProvider()
                val currentOsdMillis = currentFrame.osdMillis()
                if (currentOsdMillis + 100 < videoPositionMillis) {
                    //Seek forward in osd frames
                    while (currentFrame.osdMillis() < videoPositionMillis && frameIterator.hasNext()) {
                        currentFrame = frameIterator.next()
                    }
                } else if (currentOsdMillis - 100 > videoPositionMillis) {
                    //Seek backward in osd frames
                    while (currentFrame.osdMillis() > videoPositionMillis && frameIterator.hasPrevious()) {
                        currentFrame = frameIterator.previous()
                    }
                }
                frame = currentFrame
            }
        }
    }
    Spacer(modifier.drawWithCache {
        val scaleX = size.width / (osdRecord.charWidth * osdFont.width)
        val scaleY = size.height / (osdRecord.charHeight * osdFont.height)
        val scale = min(scaleX, scaleY)
        log("record       size: ${osdRecord.charWidth}x${osdRecord.charHeight}")
        log("record pixel size: ${osdRecord.charWidth*osdFont.width}x${osdRecord.charHeight*osdFont.height}")
        log("canvas       size: ${size.width}x${size.height}")
        log("scale            : $scaleX,$scaleY")
        val osdWidth = (osdFont.width * osdRecord.charWidth).toFloat()
        val osdHeight = (osdFont.height * osdRecord.charHeight).toFloat()
        val leftOffset = max(0f, (size.width - osdWidth * scale) / 2f)
        val topOffset = max(0f, (size.height - osdHeight  * scale) / 2f)
        onDrawBehind {
            translate(left = leftOffset, top = topOffset) {
                scale(scale = scale, pivot = Offset.Zero) {
                    frame?.let { frame ->
                        for (y in 0 until osdRecord.charHeight) {
                            for (x in 0 until osdRecord.charWidth) {
                                val xOffset = x * osdFont.width
                                val yOffset = y * osdFont.height

                                val char = frame.data[y + x * 22]
                                drawCharacter(osdFont, char, xOffset, yOffset)
                            }
                        }
                    }
                }
            }
        }
    })
}

