package de.appsonair.wtf.osdplayer.ui.compose

import android.content.Intent
import android.net.Uri
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import de.appsonair.tools.log

@Composable
fun rememberDirectoryPicker(onDirectorySelected: (Uri?) -> Unit): DirectoryPickerState {
    val ctx = LocalContext.current
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.OpenDocumentTree(),
        onResult = { uri: Uri? ->
            log("directory selected: $uri")
            if (uri != null) {
                val flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                ctx.contentResolver.takePersistableUriPermission(uri, flags)
            }
            onDirectorySelected(uri)
        }
    )
    return remember { MutableDirectoryPickerState(launcher) }
}

interface DirectoryPickerState {
    fun launchDirectoryPickerIntent(uri: Uri? = null)
}

private class MutableDirectoryPickerState(
    var launcher: ManagedActivityResultLauncher<Uri?, Uri?>
) : DirectoryPickerState {
    override fun launchDirectoryPickerIntent(uri: Uri?) {
        launcher.launch(uri)
    }
}

@Composable
fun rememberFilePicker(): FilePickerState {
    val state = remember { MutableFilePickerState() }
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.OpenDocument(),
        onResult = { uri: Uri? ->
            state.selectedUri = uri
        }
    )
    state.launcher = launcher
    return state
}

interface FilePickerState {
    val selectedUri: Uri?
    fun launchFilePickerIntent(filter: Array<String>)
}

private class MutableFilePickerState() : FilePickerState {
    var launcher: ManagedActivityResultLauncher<Array<String>, Uri?>? = null
    override var selectedUri: Uri? by mutableStateOf(null)
    override fun launchFilePickerIntent(filter: Array<String>) {
        checkNotNull(launcher).launch(filter)
    }
}
