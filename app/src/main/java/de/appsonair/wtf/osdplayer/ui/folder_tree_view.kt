package de.appsonair.wtf.osdplayer.ui

import android.content.res.Configuration
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.*
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.NavigateBefore
import androidx.compose.material.icons.filled.CreateNewFolder
import androidx.compose.material.icons.filled.Folder
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.appsonair.wtf.osdplayer.loadOsdFont
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.FontVariant
import de.appsonair.wtf.osdplayer.R
import de.appsonair.wtf.osdplayer.ui.compose.ProgressOverlay
import de.appsonair.wtf.osdplayer.ui.compose.rememberDirectoryPicker
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSTypes
import de.appsonair.wtf.osdplayer.ui.theme.normalPadding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL,
    device = "spec:parent=pixel_5,orientation=landscape"
)
@Composable
private fun PreviewFolderTreeScreen() {
    PreviewTemplateRoot {
        val mockFolderState = mockFolderTreeState(
            mockUpFolder,
            mockFolder("fpv videos 1"),
            mockFolder("videos"),
            mockFolder("Fonts"),
            mockVideo("Video 1", R.drawable.video_thumb1),
            mockVideo("Video 2", R.drawable.video_thumb2),
            mockVideo("Video 3", R.drawable.video_thumb3, false),
            mockVideo("Video 4", R.drawable.video_thumb4),
            mockFontFolder("BF font"),
            mockFontFolder("iNav font", FontVariant.ARDUPILOT),
            mockVideo("Video 5", R.drawable.video_thumb1),
            mockVideo("Video 6", R.drawable.video_thumb2),
        )
        FolderTreeScreen(
            state = mockFolderState,
            onFileSelected = {}
        )
    }
}

@Composable
fun FolderTreeScreen(
    state: FolderTreeState,
    modifier: Modifier = Modifier,
    onFileSelected: (FileItem) -> Unit
) {
    val ctx = LocalContext.current
    val directoryPicker = rememberDirectoryPicker(
        onDirectorySelected = { it?.let {
            state.switchFolder(getRootFolder(ctx))
        } }
    )
    BackHandler(enabled = state.currentFolder.parent != null) {
        state.back()
    }
    val contentPadding = WindowInsets.safeDrawing.asPaddingValues() + PaddingValues(normalPadding)
    Box {
        LazyVerticalGrid(
            modifier = modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background),
            state = state.gridState,
            columns = GridCells.Adaptive(160.dp),
            contentPadding = contentPadding,
            verticalArrangement = Arrangement.spacedBy(normalPadding),
            horizontalArrangement = Arrangement.spacedBy(normalPadding)
        ) {
            items(items = state.fileList) { item ->
                val aspectRatioModifier = Modifier.aspectRatio(1.33333f)
                when (item) {
                    is FileItem.AddFolder -> {
                        FolderItemView(
                            modifier = aspectRatioModifier,
                            icon = Icons.Default.CreateNewFolder,
                            text = stringResource(id = R.string.screen_folder_tree_add),
                            onClick = { directoryPicker.launchDirectoryPickerIntent() }
                        )
                    }

                    is FileItem.Folder -> FolderItemView(
                        modifier = aspectRatioModifier,
                        icon = Icons.Default.Folder,
                        text = item.name,
                        onClick = { state.switchFolder(item) }
                    )

                    is FileItem.UpFolder -> FolderItemView(
                        modifier = aspectRatioModifier,
                        icon = Icons.AutoMirrored.Default.NavigateBefore,
                        text = stringResource(R.string.screen_folder_tree_up),
                        onClick = { state.switchFolder(item.folder) }
                    )

                    is FileItem.FontFile -> FontItemView(
                        modifier = aspectRatioModifier,
                        font = item,
                        onClick = { onFileSelected(item) }
                    )

                    is FileItem.VideoFile -> VideoItemView(
                        modifier = aspectRatioModifier,
                        item = item,
                        onClick = {
                            log("File selected: $item")
                            onFileSelected(item)
                        }
                    )
                }
            }
        }

        // Top status bar scrim
        val background = Brush.verticalGradient(
            0.5f to MaterialTheme.colorScheme.background.copy(alpha = 0.8f),
            1f to Color.Transparent
        )
        Spacer(
            Modifier
                .align(Alignment.TopCenter)
                .fillMaxWidth()
                .windowInsetsTopHeight(WindowInsets.systemBars)
                .background(background)
        )

        //Bottom navigation bar scrim
        val backgroundInverted = Brush.verticalGradient(
            0f to Color.Transparent,
            0.5f to MaterialTheme.colorScheme.background.copy(alpha = 0.8f),
        )
        Spacer(
            Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
                .windowInsetsBottomHeight(WindowInsets.systemBars)
                .background(backgroundInverted)
        )
        ProgressOverlay(state.progressState)
    }
}

@Composable
fun FolderItemView(
    icon: ImageVector,
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.clickable(onClick = onClick),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            imageVector = icon,
            contentDescription = null,
            contentScale = ContentScale.Fit,
            colorFilter = ColorFilter.tint(LocalContentColor.current)
        )
        Text(
            modifier = Modifier.padding(8.dp),
            text = text,
            style = WTFOSTypes.folderTitle()
        )
    }
}

@Composable
fun FontItemView(
    font: FileItem.FontFile,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    val ctx = LocalContext.current
    var sampleImage: ImageBitmap? by remember { mutableStateOf(null) }
    if (isPreviewMode()) {
        sampleImage = loadOsdFont(
            variant = font.variant,
            input1 = ctx.assets.open(font.variant.fileName1()),
            input2 = ctx.assets.open(font.variant.fileName2())
        ).extractSample()
    }
    LaunchedEffect(key1 = font) {
        withContext(Dispatchers.IO) {
            sampleImage = loadOsdFont(ctx, font).extractSample()
        }
    }
    Column(
        modifier = modifier.clickable(onClick = onClick),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier.padding(8.dp),
            text = font.name,
            style = WTFOSTypes.folderTitle()
        )
        sampleImage?.let { image ->
            Image(bitmap = image, contentDescription = null)
        }
    }
}