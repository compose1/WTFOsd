package de.appsonair.wtf.osdplayer.ui.compose

import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.zIndex
import de.appsonair.tools.log
import de.appsonair.wtf.osdplayer.ui.PreviewTemplateRoot
import de.appsonair.wtf.osdplayer.ui.isPreviewMode
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock


class ProgressOverlayState {
    private val scope = MainScope()
    private val mutex = Mutex()
    private var runningJob: Job? = null

    var isRunning by mutableStateOf(false)
        private set

    fun runBlocking(block: suspend CoroutineScope.() -> Unit) {
        runBlocking {
            executeOperation(block)
        }
    }


    /**
     * Cancel all running jobs and enqueue execution of block()
     */
    fun cancelAndEnqueue(block: suspend CoroutineScope.() -> Unit) {
        scope.launch {
            runningJob?.cancel()
            executeOperation(block)
        }
    }

    /**
     * Cancel all running jobs and run block()
     */
    suspend fun cancelAndRun(block: suspend CoroutineScope.() -> Unit) {
        runningJob?.cancel()
        executeOperation(block)
    }

    /**
     * Enqueue job and return.
     */
    fun enqueueOperation(block: suspend CoroutineScope.() -> Unit) {
        scope.launch {
            executeOperation(block = block)
        }
    }

    suspend fun executeOperation(block: suspend CoroutineScope.() -> Unit) {
        mutex.withLock {
            runningJob = scope.launch {
                isRunning = true
                block(this)
                isRunning = false
            }.also {
                it.join()
            }
        }
    }
}

@Composable
fun ProgressOverlay(state: ProgressOverlayState) {
    ProgressOverlay(isVisible = state.isRunning)
}

@Preview
@Composable
private fun PreviewProgressOverlay() {
    PreviewTemplateRoot(showSystemUi = false) {
        ProgressOverlay(isVisible = true)
    }
}

/**
 * Overlay to prevent clicking area behind this overlay.
 * Also when the state is changed to Loading it will darken the background after 300 ms
 * When the state is Ready it will show the content with darkened background
 */
@Composable
fun ProgressOverlay(
    isVisible: Boolean,
    modifier: Modifier = Modifier
) {
    val previewMode = isPreviewMode()
    var visible by remember { mutableStateOf(isVisible) }
    var loadingOverlay by remember { mutableStateOf(previewMode && isVisible) }
    if (previewMode.not()) {
        LaunchedEffect(isVisible) {
            visible = isVisible
            loadingOverlay = if (isVisible) {
                delay(300)
                true
            } else {
                false
            }
        }
    }
    val alpha: Float by animateFloatAsState(
        targetValue = if (loadingOverlay) 0.7f else 0.0f,
        label = "alpha"
    )
    if (visible) {
        Box(
            modifier = modifier
                .fillMaxSize()
                .zIndex(1000f)
                .background(Color.Black.copy(alpha))
                .then( // catches all pointer events to prevent clicking
                    Modifier.pointerInput(visible) {
                        awaitPointerEventScope {
                            val event = awaitPointerEvent()
                            log(event.toString())
                        }
                    }
                ),
            contentAlignment = Alignment.Center
        ) {
            if (previewMode) {
                CircularProgressIndicator(progress = { 0.8f })
            } else {
                Crossfade(
                    targetState = loadingOverlay,
                    label = "cross fade"
                ) {
                    if (it) CircularProgressIndicator()
                }
            }
        }
    }
}
