package de.appsonair.wtf.osdplayer

import android.content.Context
import android.content.SharedPreferences
import de.appsonair.wtf.osdplayer.ui.FileItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.buffer
import okio.sink
import okio.source
import java.io.File

class FontRepository(
    val ctx: Context
) {
    private val prefs = ctx.getSharedPreferences("font_repository", Context.MODE_PRIVATE)

    // Copy font to app directory
    suspend fun importFont(font: FileItem.FontFile) {
        withContext(Dispatchers.IO) {
            var dest = File(ctx.filesDir, font.variant.fileName1())
            ctx.contentResolver.openInputStream(font.file1.uri)?.use { inputStream ->
                dest.sink().buffer().use { buffer -> buffer.writeAll(inputStream.source())}
            }
            if (font.file2 != null) {
                dest = File(ctx.filesDir, font.variant.fileName2())
                ctx.contentResolver.openInputStream(font.file2.uri)?.use { inputStream ->
                    dest.sink().buffer().use { buffer -> buffer.writeAll(inputStream.source()) }
                }
            }
            prefs.edit {
                putString(font.variant.name, font.file1.uri.toString())
            }
        }
    }

    suspend fun loadFont(variant: FontVariant): OsdFont = withContext(Dispatchers.IO) {
        val file1 = File(ctx.filesDir, variant.fileName1())
        val file2 = File(ctx.filesDir, variant.fileName2())
        if (file1.exists()) {
            val input2 = if (file2.exists()) file2.inputStream() else null
            // Load font from app storage
            loadOsdFont(variant, file1.inputStream(), input2)
        } else {
            // Load font from assets
            loadOsdFont(variant, ctx.assets.open(variant.fileName1()), ctx.assets.open(variant.fileName2()))
        }
    }

    fun isFontActive(font: FileItem.FontFile): Boolean {
        val activeUri = prefs.getString(font.variant.name, null)
        return activeUri == font.file1.uri.toString()
    }

}


fun SharedPreferences.edit(block: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    block(editor)
    editor.apply()
}