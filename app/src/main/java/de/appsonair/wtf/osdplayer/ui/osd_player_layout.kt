package de.appsonair.wtf.osdplayer.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.movableContentOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSDTheme

@Preview(
    name = "Portrait long",
    device = "spec:width=411dp,height=891dp"
)
@Preview(
    name = "Portrait tablet",
    device = "spec:width=800dp,height=1280dp,dpi=240"
)
@Preview(
    name = "Landscape long",
    device = "spec:width=891dp,height=411dp"
)
@Preview(
    name = "Portrait tablet",
    device = "spec:width=1280dp,height=800dp,dpi=240"
)
annotation class VideoScreenSizesPreviews

@VideoScreenSizesPreviews
@Composable
private fun PreviewScaffold4_3() {
    PreviewOsdPlayerScaffold(aspect = 1.33333f, gps = false)
}
@VideoScreenSizesPreviews
@Composable
private fun PreviewScaffold16_9() {
    PreviewOsdPlayerScaffold(aspect = 1.77778f, gps = false)
}

@VideoScreenSizesPreviews
@Composable
private fun PreviewScaffold4_3GPS() {
    PreviewOsdPlayerScaffold(aspect = 1.33333f, gps = true)
}
@VideoScreenSizesPreviews
@Composable
private fun PreviewScaffold16_9GPS() {
    PreviewOsdPlayerScaffold(aspect = 1.77778f, gps = true)
}

@Composable
fun PreviewOsdPlayerScaffold(
    aspect: Float,
    gps: Boolean,
    modifier: Modifier = Modifier,
    overlay: (@Composable () -> Unit)? = null
) {
    WTFOSDTheme {
        OsdPlayerScaffold(
            modifier = modifier.fillMaxSize(),
            videoAspectRatio = aspect,
            showControlOverlay = overlay != null,
            showGps = gps,
            videoPlayer = {
                Box(
                    modifier = Modifier
                    .fillMaxSize()
                    .background(Color.Green),
                    contentAlignment = Alignment.Center
                ) {
                    Text("Video", style = MaterialTheme.typography.titleMedium)
                }
            },
            gpsMap = {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color.Cyan),
                    contentAlignment = Alignment.Center
                ) {
                    Text("Map", style = MaterialTheme.typography.titleMedium)
                }
            },
            controlOverlay = overlay ?: {}
        )
    }
}

@Composable
fun OsdPlayerScaffold(
    videoAspectRatio: Float,
    showControlOverlay: Boolean,
    showGps: Boolean,
    modifier: Modifier = Modifier,
    videoPlayer: @Composable () -> Unit = {},
    gpsMap: @Composable () -> Unit = {},
    controlOverlay: @Composable () -> Unit = {}
) {
    val mapContent = remember(gpsMap as Any) { movableContentOf(gpsMap) }
    val videoContent = remember(videoPlayer as Any) { movableContentOf(videoPlayer) }

    Box(
        modifier = modifier
            .fillMaxSize()
            .background(Color.Black),
        contentAlignment = Alignment.Center
    ) {
        BoxWithConstraints(Modifier.fillMaxSize()) {
            val width = maxWidth
            val height = maxHeight
            if (width > height * videoAspectRatio) {
                // Landscape mode
                if (showGps.not()) {
                    Box(
                        Modifier
                            .align(Alignment.Center)
                            .aspectRatio(videoAspectRatio)
                            .fillMaxHeight()
                    ) {
                        videoContent()
                    }
                } else { // GPS on the side
                    Row(Modifier.fillMaxSize(), verticalAlignment = Alignment.CenterVertically) {
                        Box(
                            Modifier
                                .widthIn(max = width - 150.dp)
                                .aspectRatio(videoAspectRatio)
                                .fillMaxHeight()
                        ) {
                            videoContent()
                        }
                        Box(
                            Modifier
                                .fillMaxHeight()
                                .weight(1f, fill = false)
                                .fillMaxHeight()
                        ) {
                            mapContent()
                        }
                    }
                }
            } else { // Portrait mode
                if (showGps.not()) {
                    Box(
                        Modifier
                            .align(Alignment.Center)
                            .aspectRatio(videoAspectRatio)
                            .fillMaxWidth()
                    ) {
                        videoContent()
                    }
                } else { // With gps map at the bottom
                    Column(Modifier.fillMaxSize()) {
                        Box(
                            Modifier
                                .align(Alignment.CenterHorizontally)
                                .heightIn(max = height - 150.dp)
                                .aspectRatio(videoAspectRatio)
                                .fillMaxWidth()
                        ) {
                            videoContent()
                        }
                        Box(
                            Modifier
                                .fillMaxWidth()
                                .weight(1f)
                        ) {
                            mapContent()
                        }
                    }
                }
            }
        }
        AnimatedVisibility(
            visible = showControlOverlay,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            controlOverlay()
        }
    }
}
