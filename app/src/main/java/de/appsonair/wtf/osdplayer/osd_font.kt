package de.appsonair.wtf.osdplayer

import android.content.Context
import android.graphics.Bitmap
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import de.appsonair.wtf.osdplayer.ui.FileItem
import java.io.InputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder


/**

msp-osd source: osd_dji_overlay_udp.c

#define FONT_VARIANT_GENERIC 0
#define FONT_VARIANT_BETAFLIGHT 1
#define FONT_VARIANT_INAV 2
#define FONT_VARIANT_ARDUPILOT 3
#define FONT_VARIANT_KISS_ULTRA 4

switch (font_variant)
{
    case FONT_VARIANT_BETAFLIGHT:
        snprintf(name_buf, len, "%s_bf", font_path);
        break;
    case FONT_VARIANT_INAV:
        snprintf(name_buf, len, "%s_inav", font_path);
        break;
    case FONT_VARIANT_ARDUPILOT:
        snprintf(name_buf, len, "%s_ardu", font_path);
        break;
    case FONT_VARIANT_KISS_ULTRA:
        snprintf(name_buf, len, "%s_ultra", font_path);
        break;
    default:
        snprintf(name_buf, len, "%s", font_path);
}
 */

enum class FontVariant(val variant: Int, val suffix: String, val logoOffset: LogoOffset? = null) {
    GENERIC(0, "", LogoOffset(24, 4, 160)),
    BETAFLIGHT(1, "_bf", LogoOffset(24, 4, 160)),
    INAV(2, "_inav", LogoOffset(10, 4, 257)), // cols for iNav 6 (for iNav 5 cols=6)
    ARDUPILOT(3, "_ardu", LogoOffset(6, 4, 257)),
    KISS_ULTRA(4, "_ultra");

    companion object {
        fun fromVariant(variant: Int) = values()[variant.coerceIn(0, values().size - 1)]
    }
    fun fileName1() = "font$suffix.bin"
    fun fileName2() = "font${suffix}_2.bin"

    data class LogoOffset(
        val cols: Int,
        val rows: Int,
        val offset: Int
    )
}

private fun convertColor(data: Int): Int {
    val r = data and 0xff
    val g = (data shr 8) and 0xff
    val b = (data shr 16) and 0xff
    val a = (data shr 24) and 0xff
    return (a shl 24) + (r shl 16) + (g shl 8) + b
}

private fun loadIntArray(inputStream: InputStream): IntArray {
    inputStream.use {
        val byteData = it.readBytes()
        val buffer = ByteBuffer.wrap(byteData).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer()
        return IntArray(36 * 256 * 54) {
            convertColor(buffer.get())
        }
    }
}

private const val fontWidth = 36
private const val fontHeight = 54
private const val maxCharacters = 512
private const val bitmapColumns = 32
private const val bitmapRows = maxCharacters / bitmapColumns // 16

fun loadOsdFont(ctx: Context, file: FileItem.FontFile): OsdFont {
    val inputStream1 = checkNotNull(ctx.contentResolver.openInputStream(file.file1.uri))
    val inputStream2 = file.file2?.let { ctx.contentResolver.openInputStream(file.file2.uri) }
    return loadOsdFont(file.variant, inputStream1, inputStream2)
}

fun loadOsdFont(variant: FontVariant, input1: InputStream, input2: InputStream?): OsdFont {
    val bitmapWidth = fontWidth * bitmapColumns
    val bitmapHeight = fontHeight * bitmapRows
    val bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888)

    loadIntArray(input1).let { array ->
        for (i in 0 until bitmapColumns / 2) {
            val offset = i * (fontWidth * fontHeight * bitmapRows)
            val xOffset = i * fontWidth
            bitmap.setPixels(array, offset, fontWidth, xOffset, 0, fontWidth, bitmapRows * fontHeight)
        }
    }
    if (input2 != null) {
        loadIntArray(input2).let { array ->
            for (i in 0 until bitmapColumns / 2) {
                val offset = i * (fontWidth * fontHeight * bitmapRows)
                val xOffset = (i + bitmapColumns / 2) * fontWidth
                bitmap.setPixels(array, offset, fontWidth, xOffset, 0, fontWidth, bitmapRows * fontHeight)
            }
        }
    }
    return OsdFont(variant, bitmap.asImageBitmap(), fontWidth, fontHeight)
}


data class OsdFont(val variant: FontVariant, val image: ImageBitmap, val width: Int, val height: Int)

fun OsdFont.offset(c: Short): IntOffset {
    val column = c / bitmapRows
    val row = c % bitmapRows
    val xOffset = width * column
    val yOffset = height * row
    return IntOffset(xOffset, yOffset)
}

fun DrawScope.drawCharacter(font: OsdFont, c: Short, x: Int, y: Int) {
    val size = IntSize(font.width, font.height)
    drawImage(
        image = font.image,
        srcOffset = font.offset(c),//IntOffset(0, font.height * c),
        srcSize = size,
        dstOffset = IntOffset(x, y),
        dstSize = size,
        blendMode = BlendMode.SrcOver
    )
}
private val paint = Paint()
fun Canvas.drawCharacter(font: OsdFont, c: Short, x: Int, y: Int) {
    val size = IntSize(font.width, font.height)
    drawImageRect(
        image = font.image,
        srcOffset = font.offset(c),//IntOffset(0, font.height * c),
        srcSize = size,
        dstOffset = IntOffset(x, y),
        dstSize = size,
        paint = paint
    )
}

fun DrawScope.drawString(font: OsdFont, text: String, xOffset: Int, yOffset: Int) {
    text.forEachIndexed { i, c ->
        val x = i * font.width + xOffset
        val y = yOffset
        drawCharacter(font, c.code.toShort(), x, y)
    }
}