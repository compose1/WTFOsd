package de.appsonair.wtf.osdplayer.ui.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

/**
 * This part is copied from the LifecycleEffect.kt file from androidx.lifecycle.compose:LifecycleResumePauseEffectScope:2.7.0-alpha01
 * Should be deleted when we update to compose 1.6.0
 */

@Composable
fun LifecycleResumeEffect(
    key1: Any?,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    effects: LifecycleResumePauseEffectScope.() -> LifecyclePauseOrDisposeEffectResult
) {
    val lifecycleResumePauseEffectScope = remember(key1) {
        LifecycleResumePauseEffectScope(lifecycleOwner.lifecycle)
    }
    LifecycleResumeEffectImpl(lifecycleOwner, lifecycleResumePauseEffectScope, effects)
}

/**
 * Schedule a pair of effects to run when the [Lifecycle] receives either a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] (or any new unique
 * value of [key1] or [key2]). The ON_RESUME effect will be the body of the
 * [effects] block and the ON_PAUSE effect will be within the
 * (onPauseOrDispose clause)[LifecycleResumePauseEffectScope.onPauseOrDispose]:
 *
 * LifecycleResumeEffect(lifecycleOwner) {
 *     // add ON_RESUME effect here
 *
 *     onPauseOrDispose {
 *         // add clean up for work kicked off in the ON_RESUME effect here
 *     }
 * }
 *
 * @sample androidx.lifecycle.compose.samples.lifecycleResumeEffectSample
 *
 * A [LifecycleResumeEffect] **must** include an
 * [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] clause as
 * the final statement in its [effects] block. If your operation does not require
 * an effect for _both_ [Lifecycle.Event.ON_RESUME] and [Lifecycle.Event.ON_PAUSE],
 * a [LifecycleEventEffect] should be used instead.
 *
 * A [LifecycleResumeEffect]'s _key_ is a value that defines the identity of the effect.
 * If a key changes, the [LifecycleResumeEffect] must
 * [dispose][LifecycleResumePauseEffectScope.onPauseOrDispose] its current [effects] and
 * reset by calling [effects] again. Examples of keys include:
 *
 * * Observable objects that the effect subscribes to
 * * Unique request parameters to an operation that must cancel and retry if those parameters change
 *
 * This function uses a [LifecycleEventObserver] to listen for when [LifecycleResumeEffect]
 * enters the composition and the effects will be launched when receiving a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] event, respectively. If the
 * [LifecycleResumeEffect] leaves the composition prior to receiving an [Lifecycle.Event.ON_PAUSE]
 * event, [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] will be called
 * to clean up the work that was kicked off in the ON_RESUME effect.
 *
 * This function should **not** be used to launch tasks in response to callback
 * events by way of storing callback data as a [Lifecycle.State] in a [MutableState].
 * Instead, see [currentStateAsState] to obtain a [State<Lifecycle.State>][State]
 * that may be used to launch jobs in response to state changes.
 *
 * @param key1 A unique value to trigger recomposition upon change
 * @param key2 A unique value to trigger recomposition upon change
 * @param lifecycleOwner The lifecycle owner to attach an observer
 * @param effects The effects to be launched when we receive the respective event callbacks
 */
@Composable
fun LifecycleResumeEffect(
    key1: Any?,
    key2: Any?,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    effects: LifecycleResumePauseEffectScope.() -> LifecyclePauseOrDisposeEffectResult
) {
    val lifecycleResumePauseEffectScope = remember(key1, key2) {
        LifecycleResumePauseEffectScope(lifecycleOwner.lifecycle)
    }
    LifecycleResumeEffectImpl(lifecycleOwner, lifecycleResumePauseEffectScope, effects)
}

/**
 * Schedule a pair of effects to run when the [Lifecycle] receives either a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] (or any new unique
 * value of [key1] or [key2] or [key3]). The ON_RESUME effect will be the body
 * of the [effects] block and the ON_PAUSE effect will be within the
 * (onPauseOrDispose clause)[LifecycleResumePauseEffectScope.onPauseOrDispose]:
 *
 * LifecycleResumeEffect(lifecycleOwner) {
 *     // add ON_RESUME effect here
 *
 *     onPauseOrDispose {
 *         // add clean up for work kicked off in the ON_RESUME effect here
 *     }
 * }
 *
 * @sample androidx.lifecycle.compose.samples.lifecycleResumeEffectSample
 *
 * A [LifecycleResumeEffect] **must** include an
 * [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] clause as
 * the final statement in its [effects] block. If your operation does not require
 * an effect for _both_ [Lifecycle.Event.ON_RESUME] and [Lifecycle.Event.ON_PAUSE],
 * a [LifecycleEventEffect] should be used instead.
 *
 * A [LifecycleResumeEffect]'s _key_ is a value that defines the identity of the effect.
 * If a key changes, the [LifecycleResumeEffect] must
 * [dispose][LifecycleResumePauseEffectScope.onPauseOrDispose] its current [effects] and
 * reset by calling [effects] again. Examples of keys include:
 *
 * * Observable objects that the effect subscribes to
 * * Unique request parameters to an operation that must cancel and retry if those parameters change
 *
 * This function uses a [LifecycleEventObserver] to listen for when [LifecycleResumeEffect]
 * enters the composition and the effects will be launched when receiving a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] event, respectively. If the
 * [LifecycleResumeEffect] leaves the composition prior to receiving an [Lifecycle.Event.ON_PAUSE]
 * event, [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] will be called
 * to clean up the work that was kicked off in the ON_RESUME effect.
 *
 * This function should **not** be used to launch tasks in response to callback
 * events by way of storing callback data as a [Lifecycle.State] in a [MutableState].
 * Instead, see [currentStateAsState] to obtain a [State<Lifecycle.State>][State]
 * that may be used to launch jobs in response to state changes.
 *
 * @param key1 A unique value to trigger recomposition upon change
 * @param key2 A unique value to trigger recomposition upon change
 * @param key3 A unique value to trigger recomposition upon change
 * @param lifecycleOwner The lifecycle owner to attach an observer
 * @param effects The effects to be launched when we receive the respective event callbacks
 */
@Composable
fun LifecycleResumeEffect(
    key1: Any?,
    key2: Any?,
    key3: Any?,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    effects: LifecycleResumePauseEffectScope.() -> LifecyclePauseOrDisposeEffectResult
) {
    val lifecycleResumePauseEffectScope = remember(key1, key2, key3) {
        LifecycleResumePauseEffectScope(lifecycleOwner.lifecycle)
    }
    LifecycleResumeEffectImpl(lifecycleOwner, lifecycleResumePauseEffectScope, effects)
}

/**
 * Schedule a pair of effects to run when the [Lifecycle] receives either a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] (or any new unique
 * value of [keys]). The ON_RESUME effect will be the body of the [effects]
 * block and the ON_PAUSE effect will be within the
 * (onPauseOrDispose clause)[LifecycleResumePauseEffectScope.onPauseOrDispose]:
 *
 * LifecycleResumeEffect(lifecycleOwner) {
 *     // add ON_RESUME effect here
 *
 *     onPauseOrDispose {
 *         // add clean up for work kicked off in the ON_RESUME effect here
 *     }
 * }
 *
 * @sample androidx.lifecycle.compose.samples.lifecycleResumeEffectSample
 *
 * A [LifecycleResumeEffect] **must** include an
 * [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] clause as
 * the final statement in its [effects] block. If your operation does not require
 * an effect for _both_ [Lifecycle.Event.ON_RESUME] and [Lifecycle.Event.ON_PAUSE],
 * a [LifecycleEventEffect] should be used instead.
 *
 * A [LifecycleResumeEffect]'s _key_ is a value that defines the identity of the effect.
 * If a key changes, the [LifecycleResumeEffect] must
 * [dispose][LifecycleResumePauseEffectScope.onPauseOrDispose] its current [effects] and
 * reset by calling [effects] again. Examples of keys include:
 *
 * * Observable objects that the effect subscribes to
 * * Unique request parameters to an operation that must cancel and retry if those parameters change
 *
 * This function uses a [LifecycleEventObserver] to listen for when [LifecycleResumeEffect]
 * enters the composition and the effects will be launched when receiving a
 * [Lifecycle.Event.ON_RESUME] or [Lifecycle.Event.ON_PAUSE] event, respectively. If the
 * [LifecycleResumeEffect] leaves the composition prior to receiving an [Lifecycle.Event.ON_PAUSE]
 * event, [onPauseOrDispose][LifecycleResumePauseEffectScope.onPauseOrDispose] will be called
 * to clean up the work that was kicked off in the ON_RESUME effect.
 *
 * This function should **not** be used to launch tasks in response to callback
 * events by way of storing callback data as a [Lifecycle.State] in a [MutableState].
 * Instead, see [currentStateAsState] to obtain a [State<Lifecycle.State>][State]
 * that may be used to launch jobs in response to state changes.
 *
 * @param keys The unique values to trigger recomposition upon changes
 * @param lifecycleOwner The lifecycle owner to attach an observer
 * @param effects The effects to be launched when we receive the respective event callbacks
 */
@Composable
fun LifecycleResumeEffect(
    vararg keys: Any?,
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current,
    effects: LifecycleResumePauseEffectScope.() -> LifecyclePauseOrDisposeEffectResult
) {
    val lifecycleResumePauseEffectScope = remember(*keys) {
        LifecycleResumePauseEffectScope(lifecycleOwner.lifecycle)
    }
    LifecycleResumeEffectImpl(lifecycleOwner, lifecycleResumePauseEffectScope, effects)
}

@Composable
private fun LifecycleResumeEffectImpl(
    lifecycleOwner: LifecycleOwner,
    scope: LifecycleResumePauseEffectScope,
    effects: LifecycleResumePauseEffectScope.() -> LifecyclePauseOrDisposeEffectResult
) {
    DisposableEffect(lifecycleOwner, scope) {
        var effectResult: LifecyclePauseOrDisposeEffectResult? = null
        val observer = LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_RESUME -> with(scope) {
                    effectResult = effects()
                }

                Lifecycle.Event.ON_PAUSE -> effectResult?.runPauseOrOnDisposeEffect()

                else -> {}
            }
        }

        lifecycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
            effectResult?.runPauseOrOnDisposeEffect()
        }
    }
}

/**
 * Interface used for [LifecycleResumeEffect] to run the effect within the onPauseOrDispose
 * clause when an (ON_PAUSE)[Lifecycle.Event.ON_PAUSE] event is received or when cleanup is
 *  * needed for the work that was kicked off in the ON_RESUME effect.
 */
interface LifecyclePauseOrDisposeEffectResult {
    fun runPauseOrOnDisposeEffect()
}

/**
 * Receiver scope for [LifecycleResumeEffect] that offers the [onPauseOrDispose] clause to
 * couple the ON_RESUME effect. This should be the last statement in any call to
 * [LifecycleResumeEffect].
 *
 * This scope is also a [LifecycleOwner] to allow access to the
 * (lifecycle)[LifecycleResumePauseEffectScope.lifecycle] within the [onPauseOrDispose] clause.
 *
 * @param lifecycle The lifecycle being observed by this receiver scope
 */
class LifecycleResumePauseEffectScope(override val lifecycle: Lifecycle) : LifecycleOwner {
    /**
     * Provide the [onPauseOrDisposeEffect] to the [LifecycleResumeEffect] to run when the observer
     * receives an (ON_PAUSE)[Lifecycle.Event.ON_PAUSE] event or must undergo cleanup.
     */
    inline fun onPauseOrDispose(
        crossinline onPauseOrDisposeEffect: LifecycleOwner.() -> Unit
    ): LifecyclePauseOrDisposeEffectResult = object : LifecyclePauseOrDisposeEffectResult {
        override fun runPauseOrOnDisposeEffect() {
            onPauseOrDisposeEffect()
        }
    }
}
