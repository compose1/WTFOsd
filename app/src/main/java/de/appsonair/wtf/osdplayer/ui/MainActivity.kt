package de.appsonair.wtf.osdplayer.ui

import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.LaunchedEffect
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.decode.VideoFrameDecoder
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.mapbox.maps.ResourceOptionsManager
import com.mapbox.maps.TileStoreUsageMode
import de.appsonair.wtf.osdplayer.BuildConfig
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSDTheme

class App : Application(), ImageLoaderFactory {
    override fun onCreate() {
        initializeMapbox(this)
        super.onCreate()
    }
    override fun newImageLoader(): ImageLoader = ImageLoader.Builder(this)
        .components {
            add(VideoFrameDecoder.Factory())
        }
        .build()
    private fun initializeMapbox(context: Context) {
        ResourceOptionsManager.getDefault(
            context = context,
            defaultToken = BuildConfig.MAPBOX_TOKEN
        ).update {
            tileStoreUsageMode(TileStoreUsageMode.READ_ONLY)
        }
    }
}

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContent {
            WTFOSDTheme {
                val systemUiController = rememberSystemUiController()
                val transparentBackground = MaterialTheme.colorScheme.background.copy(alpha = 0f)
                LaunchedEffect(Unit) {
                    systemUiController.setNavigationBarColor(
                        color = transparentBackground,
                        navigationBarContrastEnforced = false
                    )
                }
                MainScreen(
                    onOpenUrl = {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
                    },
                    onExit = { finish() }
                )
            }
        }
    }
}
