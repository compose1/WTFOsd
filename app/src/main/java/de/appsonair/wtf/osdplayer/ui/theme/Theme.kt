package de.appsonair.wtf.osdplayer.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

val gray800 = Color(0xFF333333).copy(alpha = 0.8f)
val gray900 = Color(0xFF333333)
val rust600 = Color(0xFF886363)
val rust300 = Color(0xFFE1AFAF)
val taupe100 = Color(0xFFF0EAE2)
val taupe800 = Color(0xFF655454)
val white150 = Color.White.copy(alpha = 0.15f)
val white800 = Color.White.copy(alpha = 0.80f)
val white850 = Color.White.copy(alpha = 0.85f)

val DarkColorPalette = darkColorScheme(
    primary = Color.White,
    secondary = rust300,
    background = gray900,
    surface = white150,
    onPrimary = gray900,
    onSecondary = gray900,
    onBackground = taupe100,
    onSurface = white800
)

val LightColorPalette = lightColorScheme(
    primary = gray900,
    secondary = rust600,
    background = taupe100,
    surface = white850,
    onPrimary = Color.White,
    onSecondary = Color.White,
    onBackground = taupe800,
    onSurface = gray800
)

val shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = RoundedCornerShape(16.dp),
    large = RoundedCornerShape(8.dp)
)

val normalPadding = 8.dp

object ColorsImageOverlay {
    @Composable
    fun text() = Color.White
    @Composable
    fun shadow() = gray900
    @Composable
    fun background() = gray900.copy(alpha = 0.5f)
}

@Composable
fun WTFOSDTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }
    MaterialTheme(
        colorScheme = colors,
        typography = Typography,
        shapes = shapes,
        content = content
    )
}