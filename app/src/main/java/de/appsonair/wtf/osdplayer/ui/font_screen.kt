package de.appsonair.wtf.osdplayer.ui

import android.content.res.Configuration
import android.graphics.Bitmap
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FabPosition
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.appsonair.wtf.osdplayer.*
import de.appsonair.wtf.osdplayer.R
import kotlinx.coroutines.runBlocking
import kotlin.math.roundToInt

private val ExtendedFabSize = 48.dp

@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES or Configuration.UI_MODE_TYPE_NORMAL)
@Preview
@Composable
private fun PreviewFontCanvas() {
    PreviewTemplateRoot(showSystemUi = true) {
        val fontRepository = FontRepository(LocalContext.current)
        val font = runBlocking {
            fontRepository.loadFont(FontVariant.BETAFLIGHT)
        }
        FontScreen(
            font = font,
            logo = font.extractLogo(),
            isActive = false,
            onSelected = {}
        )
    }
}

@Composable
fun FontScreen(
    font: OsdFont,
    logo: ImageBitmap?,
    isActive: Boolean,
    onSelected: () -> Unit,
    modifier: Modifier = Modifier
) {
    Scaffold(
        modifier = modifier.fillMaxSize(),
        contentWindowInsets = WindowInsets.safeDrawing,
        floatingActionButtonPosition = FabPosition.Center,
        floatingActionButton = {
            if (isActive) {
                val extendedFabSize = 48.dp
                Surface(
                    shape = MaterialTheme.shapes.small.copy(CornerSize(percent = 50)),
                    color = MaterialTheme.colorScheme.surface,
                    contentColor = MaterialTheme.colorScheme.primary,
                    shadowElevation = 4.dp
                ) {
                    Box(
                        modifier = Modifier.defaultMinSize(minHeight = extendedFabSize),
                        contentAlignment = Alignment.Center
                    ) {
                        Row(Modifier.padding(8.dp)) {
                            Icon(imageVector = Icons.Default.Done, contentDescription = null)
                            Text(stringResource(id = R.string.screen_font_activated))
                        }
                    }
                }
            } else {
                ExtendedFloatingActionButton(
                    icon = { Icon(imageVector = Icons.Default.Done, contentDescription = null) },
                    text = { Text(stringResource(id = R.string.screen_font_use)) },
                    onClick = onSelected
                )
            }
        }
    ) { padding ->
        val spacing = 8.dp
        val contentPadding = padding + PaddingValues(bottom = ExtendedFabSize + spacing, top = spacing)
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            contentPadding = contentPadding,
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            if (logo != null) {
                item(key = "Logo") {
                    Image(
                        modifier = Modifier
                            .height(100.dp)
                            .fillMaxWidth(),
                        bitmap = logo,
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                }
            }
            item(key = "Title") {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(MaterialTheme.colorScheme.background),
                ) {
                    Text(
                        modifier = Modifier.align(Alignment.Center),
                        text = stringResource(R.string.screen_font_title_template, font.variant.name),
                        style = MaterialTheme.typography.titleLarge,
                        color = MaterialTheme.colorScheme.onBackground
                    )
                }
            }
            item(key = "Sample") {
                FontCanvas(font = font)
            }
        }
    }
}

@Composable
fun FontCanvas(
    font: OsdFont,
    modifier: Modifier = Modifier
) {
    var heightDp by remember { mutableStateOf(0.dp) }
    val baseColor = MaterialTheme.colorScheme.onBackground
    val colorA = baseColor.copy(alpha = 0.2f)
    val colorB = baseColor.copy(alpha = 0.1f)
    Canvas(
        modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp)
            .height(heightDp)
            .clipToBounds()
    ) {
        val cols = ((size.width / density) / 24).roundToInt()
        val rows = 256 * 2 / cols
        val scale = (size.width / (cols * font.width))
        heightDp = (font.height * rows * scale / density).dp
        val charSize = Size(font.width.toFloat(), font.height.toFloat())
        scale(scale, pivot = Offset.Zero) {
            for (x in 0 until cols) {
                for (y in 0 until rows) {
                    val char = (x + y * cols)
                    val xPos = x * font.width
                    val yPos = y * font.height
                    val evenY = if (y % 2 == 0) 0 else 1
                    drawRect(
                        color = if ((x + evenY) % 2 == 0) colorA else colorB,
                        topLeft = Offset(xPos.toFloat(), yPos.toFloat()),
                        size = charSize
                    )
                    drawCharacter(
                        font,
                        char.toShort(),
                        xPos,
                        yPos
                    )
                }
            }
        }
    }
}

fun OsdFont.extractSample(): ImageBitmap {
    val cols = 10
    val rows = 5
    val offset = 0
    val bitmap = Bitmap.createBitmap(cols * width, rows * height, Bitmap.Config.ARGB_8888).asImageBitmap()
    val canvas = androidx.compose.ui.graphics.Canvas(bitmap)
    for (y in 0 until rows) {
        for (x in 0 until cols) {
            val char = x + y * cols + offset
            canvas.drawCharacter(this, char.toShort(), x * width, y * height)
        }
    }
    return bitmap
}

fun OsdFont.extractLogo(): ImageBitmap? =
    variant.logoOffset?.let { extractLogo(it.cols, it.rows, it.offset) }

fun OsdFont.extractLogo(
    cols: Int,
    rows: Int,
    offset: Int
): ImageBitmap {
    val bitmap = Bitmap.createBitmap(cols * width, rows * height, Bitmap.Config.ARGB_8888).asImageBitmap()
    val canvas = androidx.compose.ui.graphics.Canvas(bitmap)
    for (y in 0 until rows) {
        for (x in 0 until cols) {
            val char = x + y * cols + offset
            canvas.drawCharacter(this, char.toShort(), x * width, y * height)
        }
    }
    return bitmap
}

@Composable
operator fun PaddingValues.plus(padding: PaddingValues): PaddingValues {
    val layoutDirection = LocalLayoutDirection.current
    return PaddingValues.Absolute(
        left = calculateLeftPadding(layoutDirection) + padding.calculateLeftPadding(layoutDirection),
        top = calculateTopPadding() + padding.calculateTopPadding(),
        right = calculateRightPadding(layoutDirection) + padding.calculateRightPadding(
            layoutDirection
        ),
        bottom = calculateBottomPadding() + padding.calculateBottomPadding()
    )
}
