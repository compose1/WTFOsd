package de.appsonair.wtf.osdplayer.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.halilibo.richtext.markdown.Markdown
import com.halilibo.richtext.ui.Heading
import com.halilibo.richtext.ui.material3.RichText
import de.appsonair.wtf.osdplayer.ui.theme.WTFOSDTheme

@ScreenSizesPreviews
@Composable
private fun PreviewDocumentation() {
    WTFOSDTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            DocumentationScreen(modifier = Modifier.fillMaxSize())
        }
    }
}

@Composable
fun DocumentationScreen(modifier: Modifier = Modifier) {
    val ctx = LocalContext.current
    val markdownText = remember {
        ctx.assets.open("docu/DOCU.md").bufferedReader().readText()
    }
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        //val span = HtmlCompat.fromHtml("<b>Bold</b>", 0)
        //val annotatedString = AnnotatedString
        //Text(text = )
        Heading1("WTF OSD Player")
        Text("Test")
        RichText {
            Heading(0, "Test")
            Markdown(content = markdownText)
        }
    }
}

@Composable
fun Heading1(
    text: String,
    modifier: Modifier = Modifier
) {
    Text(
        modifier = modifier,
        text = text,
        style = MaterialTheme.typography.titleLarge
    )
}