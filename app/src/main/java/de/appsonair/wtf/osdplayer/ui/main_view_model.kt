package de.appsonair.wtf.osdplayer.ui

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.ImageBitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.appsonair.wtf.osdplayer.FontRepository
import de.appsonair.wtf.osdplayer.OsdFont
import de.appsonair.wtf.osdplayer.loadOsdFont
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

sealed interface MainUiState {
    data object About: MainUiState
    data object ViewFolder: MainUiState
    data class ViewVideo(val file: FileItem.VideoFile): MainUiState
    data class ViewFont(
        val file: FileItem.FontFile,
        val font: OsdFont,
        var logo: ImageBitmap?
    ): MainUiState
}

class MainViewModel(private val ctx: Application): AndroidViewModel(ctx) {
    private val fontRepository = FontRepository(ctx)
    var mainUiState: MainUiState by mutableStateOf(MainUiState.About)
        private set

    var isLoading: Boolean by mutableStateOf(false)
        private set

    val folderTreeState = FolderTreeState(ctx, viewModelScope, getRootFolder(ctx))

    fun start() {
        mainUiState = MainUiState.ViewFolder
    }

    fun fileSelected(item: FileItem) {
        viewModelScope.launch { //TODO show progress overlay
            isLoading = true
            mainUiState = when (item) {
                is FileItem.VideoFile -> MainUiState.ViewVideo(item)
                is FileItem.FontFile -> {
                    withContext(Dispatchers.IO) {
                        val font = loadOsdFont(ctx, item)
                        val logo = font.extractLogo()
                        MainUiState.ViewFont(item, font, logo)
                    }
                }
                else -> return@launch //Not supported
            }
            isLoading = false
        }
    }

    suspend fun fontSelected(font: FileItem.FontFile) {
        fontRepository.importFont(font)
    }

    fun isActive(font: FileItem.FontFile) = fontRepository.isFontActive(font)

    /**
     * Returns true when the back action is consumed.
     */
    fun back(): Boolean {
        var consumed = true
        when (mainUiState) {
            is MainUiState.ViewVideo,
            is MainUiState.ViewFont -> {
                mainUiState = MainUiState.ViewFolder
            }
            is MainUiState.ViewFolder -> {
                mainUiState = MainUiState.About
            }
            else -> {
                consumed = false
            }
        }
        return consumed
    }
    override fun onCleared() {

    }
}
