package de.appsonair.wtf.osdplayer.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

object WTFOSTypes {
    @Composable
    fun thumbnailTitle() = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp,
        color = ColorsImageOverlay.text(),
        shadow = Shadow(blurRadius = 8f, color = ColorsImageOverlay.shadow())
    )
    @Composable
    fun folderTitle() = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    )
}

// Set of Material typography styles to start with
val Typography = Typography(
    titleMedium = TextStyle(
        fontSize = 24.sp
    ),
    bodyMedium = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )

)